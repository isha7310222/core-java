//check syntax and length

class TwoD{

	public static void main(String[] args){

		int[][] var1=new int[2][2]{1,2,3,4};//error:array creation with both dimension expression and initialization is illegal

		System.out.println(var1.length);
	}
}
