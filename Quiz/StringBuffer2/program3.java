//use replace method

class ReplaceDemo{

	public static void main(String[] args){

		String str=new StringBuffer("JamesGosling");//Error:incompatible types: StringBuffer cannot be converted to String

		System.out.println(str.replace('J','G'));
	}
}
