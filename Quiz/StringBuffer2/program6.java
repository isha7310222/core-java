//check capacity

class CapacityDemo{

	public static void main(String[] args){

		StringBuffer str=new StringBuffer();

		str.ensureCapacity(10);

		System.out.println(str.capacity());
	}
}
/*ensureCapacity method checks the capacity is less than the given parameter
 *if yes change the capacity otherwise not changed.*/
