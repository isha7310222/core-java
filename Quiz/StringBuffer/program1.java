//check identity hashcode of two strings

class IdentityHashCode1{

	public static void main(String[] args){

		String s1=new String("Core2web");
		String s2=new String("Core2web");

		if(System.identityHashCode(s1).equals(System.identityHashCode(s2)))//error:int cannot be dereferrenced
			System.out.println("True");
		else
			System.out.println("False");

	}
}

/*primitives are not objects so they actually do not have any 
 *member variables/methods.so we cannot do primitive.somthing().*/
