// use equals and setLength method to StringBuffer

class MethodDemo{

	public static void main(String[] args){

		StringBuffer var1=new StringBuffer("Shashi");

		StringBuffer var2=new StringBuffer("Shashi");

		System.out.println(var1.equals(var2));

		var1.setLength(3);

		System.out.println(var1);
	}
}
/*Equals method check if same objects are formed but there are two different objects
 *SetLength method limits the length of the StringBuffer*/ 

