//check if object of String and Stribuffer class are equal or not

class EqualDemo{

	public static void main(String[] args){

		StringBuffer var1=new StringBuffer("Shashi");

		String var2=new String("Shashi");

		if(var2.equals(var1)){

			System.out.println("Both are equal");
		}
		else{
			System.out.println("Both are not equal");
		}
	}
}
//Both are not equal coz they are objects of different classes.
