// print length and capacity of StringBuffer

class LengthDemo{

	public static void main(String[] args){

		StringBuffer sb=new StringBuffer();

		for(int i=0;i<18;i++){

			sb.append(i);
		}
		System.out.println(sb.length());
		System.out.println(sb.capacity());
	}
}
/* length will be 26
 * 0 to 9 single no..10length and 10 to 17..8 no of 2 digits...16len
 * total len=10+16=26
 * capacity..*/
