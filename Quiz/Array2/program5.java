//using for each loop print array
class ForDemo{

	public static void main(String[] args){

		int arr[5]={1,2,3,4,5};//error:initializing array size and element is illegal

		for(int var:arr)
			System.out.print(var);
	}
}
