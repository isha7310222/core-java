
class Parent{

	Parent(){

		System.out.println(this);

		System.out.println("In parent constructor");
	}

	void parentProperty(){

		System.out.println("Flat,gold,car");
	}
}
class Child extends Parent{

	Child(){

		 System.out.println(this);

		System.out.println("In child constructor");
	}
}
class Client{

	public static void main(String[] args){

		Parent obj1=new Parent();//Parent(obj1)
		obj1.parentProperty();//parentProperty(obj1
                 System.out.println(obj1);

		Child obj2=new Child();//Child(obj2)
		obj2.parentProperty();//parentProperty(obj2)
		System.out.println(obj2);		      

		Parent obj3=new Child();//Child(obj2)
		obj3.parentProperty();//parentProperty(obj3)
		System.out.println(obj3);
	}
}
