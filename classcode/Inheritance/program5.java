
//instance in inheritance

class Parent{

        int x=10;
        Parent(){

                System.out.println("In parent constructor");

             {
                System.out.println("inst block1");
             }

        }
        void access(){

                System.out.println("In parent instance");
        }
        {
                System.out.println("inst block2");
        }

}
class Child extends Parent{

        int y=20;

        Child(){
                System.out.println("In child constructor");

                System.out.println(x);

                 System.out.println(y);

               {
                System.out.println("inst block3");
               }

        }
         {
                System.out.println("inst block4");
        }
}
class Client{

        public static void main (String[] args){

                Child obj=new Child();//Child(obj)
                obj.access();//access(obj)
        }
}
