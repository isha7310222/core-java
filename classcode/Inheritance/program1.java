
class ICC{
	ICC(){
		System.out.println("In ICC Constructor");
	}
}
class BCCI extends ICC{

	BCCI(){

		System.out.println("In BCCCI constructor");
	}
}
class Client{

	public static void main (String[] args){

		BCCI obj=new BCCI();
	}
}

/*when object create call goes to constructor but first line in 
 *constructor is invokespecial means call goes to parent constructor 
 of that class*/

