
//instance in inheritance

class Parent{

	int x=10;
	Parent(){

		System.out.println("In parent constructor");

	}
	void access(){

		System.out.println("In parent instance");
	}

}
class Child extends Parent{

	int y=20;

	Child(){
		System.out.println("In child constructor");

		System.out.println(x);

		 System.out.println(y);

	}
	
}
class Client{

	public static void main (String[] args){

		Child obj=new Child();//Child(obj)
		obj.access();//access(obj)
	}
}
