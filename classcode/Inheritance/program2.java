//Real time example of inheritance

class Mamaearth{

	Mamaearth(){

		System.out.println("Welcome to Mamaearth!");
	}
}
class Products extends Mamaearth{

	Products(){

		System.out.println("There are various products for face and hair");

	}

	void face(){

		System.out.println("Products: 1.Facewash 2.Face serum 3.Scrub");

	}
	void hair(){

		 System.out.println("Products: 1.Shampoo 2.Hair oil 3.Hair mask");
	}
}
class Client{

	public static void main(String[] args){

		Products obj1=new Products();
		obj1.face();
		obj1.hair();

		Mamaearth obj2=new Mamaearth();

		Mamaearth obj3=new Products();

	}
}


