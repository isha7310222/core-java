//print array elements
class Demo1{
	public static void main(String[] args){
		int arr[]={10,20,30,40,50};
	       char arr2[]={'A','B','C'};
	      float arr3[]={10.5f,5.6f};
	    boolean arr4[]={true,false,true};
	     String arr5[]={"Virat","MSD"};

	     //Integer array
	     System.out.println(arr[0]);
	     System.out.println(arr[1]);
	     System.out.println(arr[2]);
	     System.out.println(arr[3]);
	     System.out.println(arr[4]);

	     //character array
	      System.out.println(arr2[0]);
	      System.out.println(arr2[1]);
	      System.out.println(arr2[2]);

	      //float array
	       System.out.println(arr3[0]);
	       System.out.println(arr3[1]);
	        
	       //boolean array
	        System.out.println(arr4[0]);
		System.out.println(arr4[1]);
		System.out.println(arr4[2]);

		//String array
		 System.out.println(arr5[0]);
		 System.out.println(arr5[1]);
		// System.out.println(arr5[2]); //runtime error:ArrayIndexOutOfBoundException
		  }
}
