//check identity hashcode of array elments
class DemoArr7{
        public static void main(String[] args){
                String arr1[]={"Payal","Isha","Tanvi","Ruchita"};
                String arr2[]={"Isha","Payal","Tanvi","Ruchita"};
                System.out.println(arr1);
                System.out.println(arr2);//create diff object diff address

                //identity hashcode
                 System.out.println(System.identityHashCode(arr1));
                 System.out.println(System.identityHashCode(arr2));

                  System.out.println(System.identityHashCode(arr1[1]));
                  System.out.println(System.identityHashCode(arr1[1]));

                   System.out.println(System.identityHashCode(arr1[3]));
                   System.out.println(System.identityHashCode(arr2[3]));//identity hashcode is same for only -128 to 127
        }
}
