//Take a 5 user input and print sum of array value
import java.io.*;
class DemoArr1{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int arr[]=new int[5];
		int arr_sum=0;
		System.out.println("Enter array elements");
		for(int i=0;i<5;i++){
			arr[i]=Integer.parseInt(br.readLine());
			arr_sum=arr_sum+arr[i];

		}
		System.out.println("sum of array="+arr_sum);
	}
}
