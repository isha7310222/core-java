//Take array elements from user print count of even no
import java.util.*;
class DemoArr2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size");
		int size=sc.nextInt();
		int count=0;
		int[] arr=new int[size];
		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
			count++;
			}
		}
		System.out.println("Even elements="+count);
	}
}
