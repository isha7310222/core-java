//string as a command line argument
class CmdArg{
	public static void main(String[] args){
			
		String arr[]={"Ruchita","Payal","Isha","Tanvi"};

		System.out.println("identityHashCode of array elements");

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));

		System.out.println("identityHashCode of Dynamic array");

	        System.out.println(System.identityHashCode(args[0]));
	        System.out.println(System.identityHashCode(args[1]));
	}
}

