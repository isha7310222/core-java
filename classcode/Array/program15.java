//check identity hashcode of array elments
class DemoArr5{
        public static void main(String[] args){
                char arr1[]={'A','B','C','D'};
                char arr2[]={'A','B','D','C'};
                System.out.println(arr1);
                System.out.println(arr2);//print same array values

                //identity hashcode
                 System.out.println(System.identityHashCode(arr1));
                 System.out.println(System.identityHashCode(arr2));

                  System.out.println(System.identityHashCode(arr1[1]));
                  System.out.println(System.identityHashCode(arr1[1]));

                   System.out.println(System.identityHashCode(arr1[3]));
                   System.out.println(System.identityHashCode(arr2[3]));//identity hashcode is same
        }
}
