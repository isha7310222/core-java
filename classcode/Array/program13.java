//Take array elements from user print count of even no and odd no
import java.util.*;
class DemoArr3{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("Enter array size");
                int size=sc.nextInt();
                int count1=0;
		int count2=0;
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                        if(arr[i]%2==0){
                        count1++;
                        }
			else{
				count2++;
			}

                }
                System.out.println("Even elements="+count1);
		System.out.println("Odd elements="+count2);
        }
}
