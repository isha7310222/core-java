//check identity hashcode of array elments
class DemoArr8{
        public static void main(String[] args){
                double arr1[]={10.5,67.5,78.9,65.7};
                double arr2[]={10.5,67.5,78.9,65.7};
                System.out.println(arr1);
                System.out.println(arr2);//create diff object diff address

                //identity hashcode
                 System.out.println(System.identityHashCode(arr1));
                 System.out.println(System.identityHashCode(arr2));

                  System.out.println(System.identityHashCode(arr1[1]));
                  System.out.println(System.identityHashCode(arr1[1]));

                   System.out.println(System.identityHashCode(arr1[3]));
                   System.out.println(System.identityHashCode(arr2[3]));//identity hashcode is same for only -128 to 127
        }
}
