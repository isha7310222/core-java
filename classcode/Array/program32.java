//address remain same after value change it work as pointer but called as reference of class instead of address
class DemoArr4{

        static void fun (int arr[]){

                for(int x:arr){
                         System.out.println(x);
                }
		 System.out.println(arr);

                for(int i=0;i<arr.length;i++){

                        arr[i]=arr[i]+50;
                }
		 System.out.println(arr);
        }
        public static void main(String[] args){

                int arr[]={50,100,150};

		 System.out.println(arr);
		
		fun(arr);

		for(int x:arr){
			 System.out.println(x);
		}
		 System.out.println(arr);
	}
}
