//take array element from user and print uisng for each loop
import java.io.*;
class Jagged1{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows");
		int size=Integer.parseInt(br.readLine());

                int arr[][]=new int[size][];

		for(int i=0;i<size;i++){

			System.out.println("Enter number of columns");

			arr[i]=new int[Integer.parseInt(br.readLine())];
		}

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        for(int j=0;j<arr[i].length;j++){

                                arr[i][j]=Integer.parseInt(br.readLine());
                        }
                }
                 for(int[]x:arr){

                         for(int y:x){

                                 System.out.print(y+" ");
                         }
                 System.out.println();
                 }
        }
}
