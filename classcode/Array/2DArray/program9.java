//take array sizes n elements from user and print array elements 
import java.io.*;
class Input{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row numbers");
		int size1=Integer.parseInt(br.readLine());

		System.out.println("Enter column numbers");
		int size2=Integer.parseInt(br.readLine());

                int arr[][]=new int[size1][size2];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        for(int j=0;j<arr[i].length;j++){

                                arr[i][j]=Integer.parseInt(br.readLine());
                        }
                }
                 for(int[]x:arr){

                         for(int y:x){

                                 System.out.print(y+" ");
                         }
                 System.out.println();
                 }
        }
}
