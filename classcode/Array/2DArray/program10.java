//3D array trial n error
class Demo3DArray{

	public static void main(String[] args){

		int [][][]arr=new int[][][]{{{1,2,3},{4,5}},{{6,7,8},{9,10}}};

		for(int[][]x:arr){

			for(int[]y:x){

				for(int z:y){

					System.out.print(z+" ");
				}
				System.out.println();
			}
			System.out.println();
		}

	}
}
