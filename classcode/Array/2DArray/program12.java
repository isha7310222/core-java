//take plane no ,rows and column from user and print the 3D array
import java.io.*;
class ThreeD{

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter plane no");
		int plane=Integer.parseInt(br.readLine());
                int row=0;
		System.out.println("Enter row number");
		for(int i=0; i<plane; i++){

		 row=Integer.parseInt(br.readLine());
		}

		int arr[][][]=new int[plane][row][];

		for(int i=0;i<plane;i++){

			for(int j=0;j<row;j++){

		        System.out.println("Enter Column number");

			arr[i][j]=new int[Integer.parseInt(br.readLine())];
			}
			
		}
		System.out.println("Enter array elements");

		for(int i=0;i<plane;i++){

		for(int j=0;j<arr[i].length;j++){

			for(int k=0;k<arr[j].length;k++){

				arr[i][j][k]=Integer.parseInt(br.readLine());
			}
		}
		}

		for(int[][]x:arr){

                        for(int[]y:x){

                                for(int z:y){

                                        System.out.print(z+" ");
                                }
                                System.out.println();
                        }
                        System.out.println();
                }

        }
}
