//check identity hashcode of 2D array
class CheckId{

	public static void main(String[] args){

		int arr[][]=new int[2][3];

		arr[0][0]=10;
		arr[0][1]=20;
		arr[0][2]=30;
		arr[1][0]=40;
		arr[1][1]=50;
		arr[1][2]=60;
		int x=20;

		System.out.println(System.identityHashCode(arr[0][0]));
		System.out.println(System.identityHashCode(arr[0][1]));
		System.out.println(System.identityHashCode(arr[0][2]));
		System.out.println(System.identityHashCode(arr[1][0]));
		System.out.println(System.identityHashCode(arr[1][1]));
	        System.out.println(System.identityHashCode(arr[1][2]));

		System.out.println(System.identityHashCode(arr));

		System.out.println(System.identityHashCode(x));

	}
}
