
class FloatDemo{{
        public static void main(String[] args){
                float x=3.5f;
                float y=3.5f;
                Float z=3.5f;

                System.out.println(System.identityHashCode(x));
                System.out.println(System.identityHashCode(y));
                System.out.println(System.identityHashCode(z));
        }
}
