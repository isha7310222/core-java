//For each loop
class ForEachDemo{
	public static void main(String[] args){
		
		int arr[]={10,20,30,40};
                
		System.out.println("Print array elements using for loop");

		for(int i=0;i<arr.length;i++){

			System.out.println(arr[i]);
		}


		//using for each loop
		System.out.println("Print array element using for each loop");
		for(int x:arr){
			System.out.println(x);
		}
	}
}
