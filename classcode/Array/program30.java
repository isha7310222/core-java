//passing array as an argument
class DemoArr2{

	static void fun(int xarr[]){

               System.out.println("In fun");
		for(int x:xarr){

		System.out.println(x);
		}
		xarr[0]=50;
	}
	public static void main(String[] args){

		int arr[]={10,20,30};
		
		for(int x:arr){

		System.out.println(x);
		}
		fun(arr);

                System.out.println("After fun");

                for(int x:arr){

                System.out.println(x);
                }
	}
}

