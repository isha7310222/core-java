
/*null value can be hold by all dattypes including object but Object is parent
so that priorty is given to child class 
OBJECT class can hold all datatypes as it is parent of all*/

class Demo{

	void fun(Object obj){

		System.out.println("Object");
	}
	void fun(String str){

		System.out.println("String");
	}
}
class Client{

	public static void main(String[] args){
	
		Demo obj=new Demo();

		obj.fun("Core2web");//String
		obj.fun(new StringBuffer("Core2web"));//Object
		obj.fun(null);   //String
	}
}
