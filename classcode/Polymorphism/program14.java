
//Ambiguity in overloading

class Demo{

	void fun(String str){

		System.out.println("String");
	}
	void fun(StringBuffer str1){

		 System.out.println("StringBuffer");
	}
}
class Client{

	public static void main(String[] args){

		Demo obj=new Demo();
		obj.fun("Core2web");//string
		obj.fun(new StringBuffer("Core2web"));//stringBuffer
//		obj.fun(null);    error: reference to fun is ambiguous
	}
}
