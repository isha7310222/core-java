//Overring in polymorphism

class Parent{

	Parent(){
		
		System.out.println("Parent Constructor");
	}
	void property(){

		System.out.println("Home,Car ,Gold");
	}
	void marry(){

		System.out.println("MSD");
	}
}
class Child extends Parent{

	Child(){

		System.out.println("Child constructor");
	}
	void marry(){

		System.out.println("Virat kohali");
	}
}
class Client{

	public static void main(String[] args){

		Child obj=new Child();
		obj.property();
		obj.marry();
	}
}
