/* static modifier in overriding
 if both classes are static is called method hiding
 static has limited access to class only thats why hide data from classes
 call goes to the class whose reference is created */


class Parent{

      static void fun(){

                System.out.println("Parent fun");
        }
}
class Child extends Parent{

        static void fun(){

         System.out.println("Child fun");
        }
}
class Client{

	public static void main(String[] args){

		Parent obj1=new Child();
		obj1.fun();
	}
}
