// static modifier in overriding


class Parent{

        void fun(){

                System.out.println("Parent fun");
        }
}
class Child extends Parent{

        static void fun(){//overriding method is static

         System.out.println("Child fun");
        }
}
// non-static cannot be converted to static
