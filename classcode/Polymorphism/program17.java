/*If return types are primitive datatypes then return type of parent-child must be same
 *only co-variant datatypes are accepted i.e. Praent-child relation
 but Parent class returntype should be parent like Object-String */

class Parent{

        Object fun(){

                System.out.println("Object fun");
                 return 'A';
        }
}
class Child extends Parent{

        String fun(){

                System.out.println("String fun");
                return "ABC";
        }
}
class Client{

        public static void main(String[] args){

                Parent p=new Child();
                p.fun(); //String fun 
        }
}
