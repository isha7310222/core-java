
//Final in overriding


class Parent{

        final void fun(){

                System.out.println("Parent fun");
        }
}
class Child extends Parent{

         void fun(){

         System.out.println("Child fun");
        }
}
// Error:  overridden method is final
