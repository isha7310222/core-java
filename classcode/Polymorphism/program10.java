class Parent{

        Parent(){

                System.out.println("Parent Constructor");
        }
        void fun(){

                System.out.println("In Parent fun");
        }
}
class Child extends Parent{

        Child(){

                System.out.println("Child constructor");
        }
        void fun(int x){

                System.out.println("In Child fun");
        }
}
class Client{

        public static void main(String[] args){

                Parent obj=new Child();

                obj.fun(10);
        }
}
/*only reference side is check by the compiler
 *compiler check method table of parent if fun()
 method is there or not it finds fun(int) not fun()
it gives error method fun in class Parent cannot be applied to given types*/                                                
