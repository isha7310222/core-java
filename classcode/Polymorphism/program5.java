//if object is of child then childs method is calles if parent and child have same methods

class Parent{

        Parent(){

                System.out.println("Parent Constructor");
        }
        void fun(){

                System.out.println("In Parent fun");
        }
}
class Child extends Parent{

        Child(){

                System.out.println("Child constructor");
        }
        void fun(){

                System.out.println("In Child fun");
        }
}
class Client{

        public static void main(String[] args){

		Parent obj=new Child();

		obj.fun();
	}
}

