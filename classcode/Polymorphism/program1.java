//method signature must be unique when using overloading
//return type does not consider in method signature as well as in overloading

class Demo{

        int x=10;
        void fun(int x){

                System.out.println(x);;
        }
        void fun(int x){//error:already defined

                System.out.println(x);;
        }
}
//method name and parameter cannot be same
