/*Access specifier in overriding
we can expand access specifier of parent i.e. from default to public
but different for private it does not participate in overloading
private methods cannot inherited*/

class Parent{

         void fun(){

                System.out.println("Parent fun");
        }
}
class Child extends Parent{

        private void fun(){

         System.out.println("Child fun");
        }
}
//Error:   attempting to assign weaker access privileges; was package
