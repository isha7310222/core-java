/*Access specifier in overriding
we can expand access specifier of parent i.e. from default to public
but different for private it does not participate in overloading 
private methods cannot inherited*/

class Parent{

        private void fun(){

                System.out.println("Parent fun");
        }
}
class Child extends Parent{

        void fun(){

         System.out.println("Child fun");
        }
}
class Client{

	public static void main(String[] args){

/*		Parent obj=new Child();  fun has private acccess
		obj.fun();  */


                Child obj1=new Child();
                obj1.fun();//child fun
	}
}

