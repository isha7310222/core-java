
//reference of abstract class can be created but not object

abstract class Parent{

	void career(){

		System.out.println("Doctor");
	}
	abstract void marry();
}
class Child extends Parent{

	void marry(){

		System.out.println("Virat");
	}
}
class Client{

	public static void main(String[] args){
		 
		Child obj1=new Child();
		obj1.career();
		obj1.marry();

		Parent obj2=new Child();
		obj2.career();
		obj2.marry();
	}
}
