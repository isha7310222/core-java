
//Real time example of abstract class

abstract class MacD{

	void price(){

		System.out.println("Prices of all items must be same");
	}
	abstract void infrastructure();
}

class Franchise extends MacD{

	void infrastructure(){

		System.out.println("6 table,chairs and small garden");
	}
}
class Client{

	public static void main(String[] args){

		MacD obj=new Franchise();
		obj.price();
		obj.infrastructure();
	}
}
