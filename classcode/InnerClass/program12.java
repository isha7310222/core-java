
//static nested Inner class

class Outer{

	void m1(){

		System.out.println("In m1 Outer");
	}
	static class Inner{

		void m1(){

                System.out.println("In m1 Inner");
		
		}

	}
	void m2(){

                System.out.println("In m2 Outer");
	}
}
class Client{

	public static void main(String[] args){

		Outer obj=new Outer();
		obj.m1();
		obj.m2();

		Outer.Inner obj1=new Outer.Inner();
		obj1.m1();
	}
}

