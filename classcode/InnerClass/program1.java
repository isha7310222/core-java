
//Normal inner class

class Outer{

	class Inner{

		void m1(){

                System.out.println("m1 Inner");
		 System.out.println(this);
		}
	}
	void m2(){

		 System.out.println("m2 Outer");
		 System.out.println(this);
	}
}
class Client{

	public static void main(String[] args){

		Outer obj=new Outer();
		obj.m2();

		Outer.Inner obj2=obj.new Inner();
		obj2.m1();

		Outer.Inner obj3=new Outer().new Inner();
                obj3.m1();
	}
}
