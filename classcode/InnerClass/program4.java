
//static nested Inner class

class Outer{

	void m1(){

		  System.out.println("Outer m1");
	}
	static class Inner{

		void m1(){

			  System.out.println("Inner m1");
		}
	}
}
class Client{

	public static void main(String[] args){

		Outer.Inner obj=new Outer.Inner();
		obj.m1();

		Outer obj1=new Outer();
		obj1.m1();
	}
}
