
//Method Local Inner class
//complusory create object inside method in which class is defined
class Outer{

	void m1(){

		  System.out.println("Outer m1");
	//method local inner class
	
	class Inner{

		void m1(){

			  System.out.println("Inner m1");
		}
	
       	}
	Inner obj=new Inner();
	obj.m1();

}
	void m2(){

		  System.out.println("Outer m2");
	}
}
class Client{

	public static void main(String[] args){

		Outer obj=new Outer();
		obj.m1();
		obj.m2();
	}
}

