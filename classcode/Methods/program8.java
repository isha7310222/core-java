// parameter and arguments of different datatypes
class Check{
	void fun(float x){
		System.out.println("In fun");
		System.out.println(x);
	}
	public static void main(String[] args){
		Check obj=new Check();
		obj.fun(10);//10.0
		obj.fun(10.5f);//10.5
	        obj.fun('A');//65.0
	        obj.fun(10.5);//posible loosy conversion from double to float
	        obj.fun(true);//boolean cannot be converted to float
	}
}


