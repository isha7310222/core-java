//non static fun cannot be called from static fun
class MethodDemo{
	static void fun(){
		System.out.println("In fun function");
	}
        static void gun(){
		System.out.println("in gun functiuon");
	}

	public static void main(String[] args){
		fun();
		gun();//error non static Method gun() cannot be referenced from a static context
	}
}

