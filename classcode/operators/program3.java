class Unary{
	public static void main(String[] args){
		int x=5;
		int y=7;
		System.out.println(++x);
		System.out.println(++y);
		System.out.println(x);
		System.out.println(y);
		System.out.println(x++);
		System.out.println(y++);
		System.out.println(x);
		System.out.println(y);

		int ans1=++x + x++;
		int ans2=--y + y--;
		System.out.println(x);
		System.out.println(y);
		System.out.println(ans1);
		System.out.println(ans2);
		int ans3=++x + y++ +x++;
		System.out.println(x);
		System.out.println(y);
		System.out.println(ans3);
	}
}
