
interface Demo1{

	static void m1(){

		System.out.println("Demo1 m1-static");
	}
	void m2();

}
class DemoChild implements Demo1{

	public void m2(){

		System.out.println("Demochild m2");
	}
}
class Client{

	public static void main(String[] args){

		Demo1 obj=new DemoChild();
		Demo1.m1();
		obj.m2();
	}
}
