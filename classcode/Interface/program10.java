
//static can be call using interface name

interface Demo1{

	static void fun(){

		System.out.println("In fun demo1");
	}
}
interface Demo2{

	static void fun(){

                System.out.println("In fun demo2");
        }
}
class Demochild implements Demo1,Demo2{

	void fun(){
	 
		System.out.println("In fun demochild");
		Demo1.fun();
		Demo2.fun();
	}
}
class Client {

	public static void main(String[] args){
/*
		Demo1 obj1=new Demochild();
		obj1.fun();                  error:static cannot be call using object

		Demo2 obj2=new Demochild();
                obj2.fun();*/

		Demochild obj3=new Demochild();
                obj3.fun();
	}
}
