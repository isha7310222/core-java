
//if multiple interfaces having same methods then ambiguity occur in this case that method should be overridden by child

interface Demo1{

	default void fun(){

		System.out.println("In fun Demo1");

	}
}
interface Demo2{

	 default void fun(){

                System.out.println("In fun Demo2");

        }
}
class DemoChild implements Demo1,Demo2{

	public void fun(){

                System.out.println("In fun DemoChild");
    }
}
class Client {

	public static void main(String[] args){

		Demo1 obj=new DemoChild();
		obj.fun();

		Demo2 obj2=new DemoChild();
		obj2.fun();

                DemoChild obj3=new DemoChild();
                obj3.fun();
	}
}

