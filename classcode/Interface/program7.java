//default in interface

interface Demo{

	void gun();
	default void fun(){

		System.out.println("In fun Demo");
	}
}
class DemoChild implements Demo{

	public void gun(){

		System.out.println("In gun Demochild");
	}
}
class Client{

	public static void main(String[] args){

		Demo obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}
//default inherit to child class
//it can be override if we want to change it
//execute mwthod whose object is created
//cannot access interface method after overriding it
