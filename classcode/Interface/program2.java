
interface Demo{
	
	void fun();
        void gun();
}
abstract class Demochild implements Demo{

	public void gun(){

		System.out.println("In gun");
	}
}
class DemoChild1 extends Demochild{

	public void fun(){

		 System.out.println("In fun");
	}
}
class Client{

	public static void main(String[] args){

		Demo obj=new DemoChild1();
		obj.gun();
		obj.fun();

		Demochild obj1=new DemoChild1();
                obj1.gun();
                obj1.fun();

		DemoChild1 obj2=new DemoChild1();
                obj2.gun();
                obj2.fun();
	}
}

