
//static in interface cannot inherit to child so it cannot be override by child
//can be access by name of interface
interface Demo{

	static void fun(){

		System.out.println("In fun");
	}
}
class DemoChild implements Demo{

}
class Client{

	public static void main(String[] args){

		DemoChild obj=new DemoChild();
//		obj.fun(); error:cannot find symbol
	}
}
