
//Variables in interface
//they are initialize in stack frame everytime it mention 

interface Demo{

	int x=10;
	void fun();
}
class DemoChild implements Demo{

	public void fun(){

		System.out.println(x);//10

		System.out.println(Demo.x);//10

	}
}
class Client{

	public static void main(String[] args){

		Demo obj=new DemoChild();
		obj.fun();
	}
}

