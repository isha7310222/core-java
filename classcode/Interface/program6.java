
class Demo1{

	static void fun(){

		System.out.println("In fun");
	}
	void gun(){

		  System.out.println("Demo1 gun");
	}

}
class Demo2 extends Demo1{

	void gun(){

		System.out.println("In gun");
	}
}
class Client{

	public static void main(String[] args){

		Demo1 obj=new Demo2();
		obj.fun();
		obj.gun();
	}
}
/* Static cannot override but it inherit in child class
 * but in interface static does not inherit in child*/
