class ThisDemo{

        int x=10;

        ThisDemo(){
 

//		this(50);....error:recursive constructor invocation
                System.out.println("No args Constructor");
        }
        ThisDemo(int x){

                this();// this should be first statement in constructor
                System.out.println("In para Constructor");
        }

        public static void main(String[] args){

                ThisDemo obj1=new ThisDemo(20);
        }
}


