//this reference

class Demo{

	int x=10;

	Demo(){     //Demo(Demo this)
		System.out.println("In constructor");
	}
	void fun(){     //fun(Demo this)

		System.out.println(x);
	}
	public static void main(String[] args){

		Demo obj=new Demo();//Demo(obj)
		obj.fun();
	}
}
/*First of all there is no static block so first call goes to main method
 *In main obj is created after creating object implicitly call goes to constructor
 object is created on heap in object constructor is stored In constructor fisrtly 
 address of special structure is stored instance varible and instance method are stored
 while pushing stackframe of constructor it pass with obj i.e address of object pass as argument
 and there is this variable pass as parameter for argument to store...same for fun..*/
