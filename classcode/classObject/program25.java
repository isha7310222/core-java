//Static and instnace methods variables blocks check sequence of execution

class Demo1{

	int x=10;
	static int y=20;

	Demo1(){

		System.out.println("Constructor");

        {
                System.out.println("In instance block 1");
        }
	}
	static{
		System.out.println("In Static block 1");


        {
                System.out.println("In instance block 2");
        }
	}
	
	{
		System.out.println("In instance block 3");
	}

	public static void main(String[] args){

		System.out.println("In main");
		Demo1 obj=new Demo1();

        {
                System.out.println("In instance block 5");
        }
	}
	static {

		System.out.println("In static block 2");
	}

	{
		System.out.println("In instance block 4");
	}
}
