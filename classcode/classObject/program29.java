class ConstructDemo{

        static int y=20;
	int x=10;

	static{
		System.out.println("In static block");
	}

	static void fun(){

		System.out.println("In static fun");
	}

	void demo(){

		System.out.println("In instance method");
	}
        ConstructDemo(){

                System.out.println("No argument");
        }
	{
		System.out.println("instance block");
	}

        ConstructDemo(int x){

                System.out.println("Parameterized");
                System.out.println(this.y);
                System.out.println(x);
        }
        ConstructDemo(ConstructDemo xyz){

                System.out.println( "Parameterized constructDemo");
                System.out.println(xyz);

        }
        public static void main(String[] args){

        ConstructDemo obj1=new ConstructDemo();

        ConstructDemo obj2=new ConstructDemo(y);

        ConstructDemo obj3=new ConstructDemo(obj1);

	fun();
	obj3.demo();
        }
}
