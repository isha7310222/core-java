
//Method signature
class Demo{

	int x=10;

	Demo(){//Demo(Demo this0
		System.out.println("In constructor");
		System.out.println(x);
	}

	/*Demo(){  error:constrctor demo is already defined in class Demo
	      
	}*/

	Demo(int x){//Demo(Demo this,int x)

		System.out.println("para constructor");

		System.out.println(x);//local variable has high priority
	       
		System.out.println(this.x);
	}

	public static void main(String[] args){

		Demo obj1=new Demo();//Demo(obj1)

		Demo obj2=new Demo(20);//Demo(obj2,20)
	}
}


