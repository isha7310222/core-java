//changing values of private variable from other class
class player{

        private int jerNo=0;
        private String name=null;

        player(int jerNo,String name){ //player(player this)
		//instance variable updation:setter method

                this.jerNo=jerNo;
                this.name=name;      //this is hidden parameter to assign local variable data into instance variable
                System.out.println("In constructor");
        }
        void info(){//info(player this)

                 System.out.println(jerNo+" = "+name);//print updated variable:getter method
        }
}
class Client{

        public static void main(String[] args){

                player obj1=new player(18,"Virat");//player(obj1,18,virat)						   //
                obj1.info();                       //info(obj1)

                player obj2=new player(7,"MSD");//player(obj2,7,MSD)
                obj2.info();                    //info(obj2)

                player obj3=new player(45,"Rohit");//player(obj3,45,Rohit)
                obj3.info();                       //info(obj3)
        }
}
