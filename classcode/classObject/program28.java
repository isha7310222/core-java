//types  of constructor

class ConstructDemo{

	static int y=20;

	ConstructDemo(){

		System.out.println("No argument");
		 System.out.println(this);
	}
	ConstructDemo(int x){

		System.out.println("Parameterized");

		this.y=30;

		System.out.println(this.y);

		System.out.println(this);
		System.out.println(x);
	}
	ConstructDemo(ConstructDemo xyz){

		System.out.println( "Parameterized constructDemo");
		System.out.println(xyz);
		System.out.println(this);
	
	}

        public static void main(String[] args){

	ConstructDemo obj1=new ConstructDemo();

        ConstructDemo obj2=new ConstructDemo(y);

	ConstructDemo obj3=new ConstructDemo(obj1);
	}
}
