
class ConstrDemo{

	ConstrDemo(){

		System.out.println("In constructor");
	}
	void fun(){

		ConstrDemo obj=new ConstrDemo();
		System.out.println(obj);

	}
	public static void main(String[] args){

		ConstrDemo obj1=new ConstrDemo();
		ConstrDemo obj2=new ConstrDemo();

		obj1.fun();

		System.out.println(obj1);
		System.out.println(obj2);
	}
}

