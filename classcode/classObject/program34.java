//changing values of private variable from other class
class player{

	private int jerNo=0;
	private String name=null;

	player(int jNo,String pName){

		jerNo=jNo;
		name=pName;
		System.out.println("In constructor");
	}
	void info(){

		 System.out.println(jerNo+" = "+name);
	}
}
class Client{

	public static void main(String[] args){

		player obj1=new player(18,"Virat");
		obj1.info();

		player obj2=new player(7,"MSD");
                obj2.info();
	}
}
