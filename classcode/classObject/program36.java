//changing values of private variable from other class
class player{

        private int jerNo=0;
        private String name=null;

        player(){
                System.out.println("In constructor");//print updated variable:getter method
        }
        void info(int jerNo,String name){//instance variable updation:setter method

                this.jerNo=jerNo;
                this.name=name;

                 System.out.println(jerNo+" = "+name);//print updated variable:getter method
        }
}
class Client{

        public static void main(String[] args){

                player obj1=new player();
		obj1.info(18,"Virat");

                player obj2=new player();
                obj2.info(7,"MSD");

                player obj3=new player();
                obj3.info(45,"Rohit");
        }
}
