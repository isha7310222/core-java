//instance block

class InstanceDemo{

	int x=10;

	void fun(){

		System.out.println("In fun");
	}

	InstanceDemo(){
		System.out.println("In constructor");
	}
	{
		System.out.println("Instance block 1");
		fun();
	}
	public static void main(String[] args){

		InstanceDemo obj=new InstanceDemo();

		System.out.println("main");
	}
	{
		System.out.println("Instance block 2");
	}
}
/*instance block is merge in constructor..
 *After creating object first instance block execute
 then constructor */
