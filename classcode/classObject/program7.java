//constructor example
class Demo{

	int x=10;
	int y=20;

	void display(){

		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String[] args){

		Demo obj=new Demo();
		obj.display();
	}

}
/*After creation of object in main constructor is created x and y are instance 
 * variables and method display store on constructor.In constructore call goes to invokespecial goes to object class
 * object class has return it return the call to constructor..constructor's stack frame is pop
 * call returns to main then call goes to display method prints lines in display return back to main
 * end of code..*/
