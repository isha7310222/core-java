//private access specifier is accesible in same class only

class C2W{

	int noOfStudents=800;

	private String courseName="Java";

	void display(){

		System.out.println(noOfStudents);

                System.out.println(courseName);
	}
}
class Student{

	public static void main(String[] args){

		C2W obj=new C2W();
		obj.display();

                System.out.println(obj.noOfStudents);
		
           //     System.out.println(obj.courseName);...error:courseName has private access in C2W
        }
}
