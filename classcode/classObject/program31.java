/* caling two different constructor from one object*/

class ThisDemo{

	int x=10;

	ThisDemo(){

		System.out.println("No args Constructor");
	}
	ThisDemo(int x){

		this();// this should be first statement in constructor
		System.out.println("In para Constructor");
	}

	public static void main(String[] args){

		ThisDemo obj1=new ThisDemo(20);
	}
}
