//creating multiple objects of same constructor

class IPL{

	IPL(){
		System.out.println("RCB");
	}
	public static void main(String[] args){

		IPL obj1=new IPL();
		IPL obj2=new IPL();
	}
}
/* two objects created on heap and call goes to constructor class
 * constructor class includes involke special method that calls object
 * class i.e. parent of all classes and call returns to constructor again 
 * execute another methods in constructor and returns to main */
