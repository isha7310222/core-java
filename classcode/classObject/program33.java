class ThisDemo{

        private int x=10;

        ThisDemo(){

		super();

                System.out.println("No args Constructor");
        }
        ThisDemo(int x){

                this();
                System.out.println("In para Constructor");
        }

        public static void main(String[] args){

                ThisDemo obj1=new ThisDemo(20);
        }
}
