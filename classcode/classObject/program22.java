
class StaticDemo{

	static{
		System.out.println("Static block 1");

	}
	public static void main(String[] args){

		System.out.println("In main");

		Client1 obj=new Client1();

	}
}
class Client1{

	static {

		System.out.println("Static block2");
	}
	public static void main(String[] args){

		System.out.println("In main");
		StaticDemo obj=new StaticDemo();
	}

	static{
		System.out.println("Static block 3");

	}
}
// After creating object call goes to static block
