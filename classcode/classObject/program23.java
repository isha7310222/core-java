
class Demo{

	static int x=10;

	static {

		//static int y=10;..error
	}
	static void fun(){

		//static int z=20;....error
	}
	void gun(){

		//static int y=20;....error
	}
}
/* Java supports global static variables
 * we cannot declared or initialized static variable 
 * in any block or scope 
 * it should be visible to all methods
 * static variable has highest priority */
