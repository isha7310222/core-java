//check if + and concat are same or not?
class StringDemo1{

	public static void main(String[] args){

		String str1="Shashi";
		String str2="Bagal";

		String str3=str1+str2;
		String str4=str1.concat(str2);

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}

}
/*Str3 and str4 are not same str3 internally calls append method of string builder class in string builder class it call to new string 
 * the value store in new string and reference of that string is given to the str2 .....str4 directly call concat method in string*/

