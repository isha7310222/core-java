//check identity hashcode of String to decide their location on heap
class StringDemo{

	public static void main(String[] args){

		String str1="Core2Web";
		String str2=new String("Core2Web");

		char str3[]={'C','2','W'};
		String str4=new String(str3);

		String str5="Core2Web";
		String str6=new String("Core2Web");

		System.out.println("IdentityHashCode of All strings");

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
                System.out.println(System.identityHashCode(str3));
                System.out.println(System.identityHashCode(str4));
	        System.out.println(System.identityHashCode(str5));
	        System.out.println(System.identityHashCode(str6));
                
		int x=67;
		System.out.println("identityHashCode of char array ");

		System.out.println(System.identityHashCode(str3[0]));
		System.out.println(System.identityHashCode(str3[1]));
		System.out.println(System.identityHashCode(str3[2]));

		System.out.println("identityHashCode of varible x");
		System.out.println(System.identityHashCode(x));
	}

}
