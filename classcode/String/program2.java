//check identity hashcode of String to decide their location on heap
class StringDemo{

        public static void main(String[] args){

                String str1="Core2Web";
                String str2=new String("Core2Web");

                char str3[]={'C','2','W'};
                String str4=new String(str3);

                String str5="Core2Web";
                String str6=new String("Core2Web");

                int x=67;

                System.out.println(System.identityHashCode(str3[0]));
                System.out.println(System.identityHashCode(str3[1]));
                System.out.println(System.identityHashCode(str3[2]));

		System.out.println("identityHashCode of Varibale x");
              
                System.out.println(System.identityHashCode(x));
        }

}
