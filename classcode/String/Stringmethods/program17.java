//end with method in string
 class EndsWithDemo{

	 public static void main(String[] args){

		 String str1="Know the code till the core";

		 System.out.println(str1.endsWith("ore"));

	 }
 }
/*if suffix is empty String true is returned.
 * throws null pointer exception if suffix is null i.e.str1=null*/
