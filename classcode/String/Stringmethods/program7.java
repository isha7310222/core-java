//length method in string
class LengthDemo{

	static int mylength(String str){

		int count=0;

		for(int i=0;i<str.length();i++){

			count++;
		}
		return count;
	}

        public static void main(String[] args){

                String str="Core2web";

                int len =mylength(str);

		System.out.println("Length of string :"+len);
        }
}
