//take two strings from user compare lengths using userdefined method

import java.io.*;

class StrLenDemo{

	static int myStrLen(String str){

		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			
			count++;
		}
		return count; 
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter first String");
		String str1=br.readLine();

		System.out.println("Enter Second String");
		String str2=br.readLine();

		int len1=myStrLen(str1);
		int len2=myStrLen(str2);

		if(len1==len2){

			System.out.println("Equal lengths");
		}
		else{
			System.out.println("Not Equal lengths");
		}
	}
}
