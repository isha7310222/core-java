
class Parent{

	int x=10;
	static int y=20;

	Parent(){

		System.out.println("In parent constructor");
	}
	void m1(){

		 System.out.println("In parent m1");
	}
	static void m2(){

		 System.out.println("In parent m2");
	}
}
class Child extends Parent{

	int A=10;
	static int B=20;

        Child(){

                 System.out.println("In Child constructor");
        }
        void m1(){

                 System.out.println("In Child m1");
        }
        static void m3(){

                 System.out.println("In Child m3");
        }
}

class Client{

	public static void main(String[] args){

		Parent obj1=new Parent();
		obj1.m1();
		obj1.m2();


                Child obj2=new Child();
                obj2.m1();
                obj2.m2();
		obj2.m3();


                Parent obj3=new Child();
                obj3.m1();
                obj3.m2();
	}
}






