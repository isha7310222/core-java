//boiler plate code use in company
class Demo{
	
	Demo(){

		System.out.println("In constructor Demo");
	}
	
}
class Demochild extends Demo{

	Demochild(){

                System.out.println("In constructor Demochild");
	}
}
class Parent{

	Parent(){

                System.out.println("In parent constructor");
	}
	Demo m1(){

                System.out.println("In m1 Demo");
		return new Demo();
	}
}
class Child extends Parent{

	Child(){

                System.out.println("In constructor child");
	}
	 Demochild m1(){

                System.out.println("In m1 Demochild");
		return new Demochild();
        }
}
class Client{
	public static void main(String[] args){

		Parent p=new Child();
		p.m1();
	}
}

