
//change thread name..during thread creation but it needs parameterized constructor
class Mythread extends Thread{

	Mythread(String str){

		super(str);
	}
	Mythread(){

	}

        public void run(){

                System.out.println(getName());
                System.out.println(Thread.currentThread());
                System.out.println(Thread.currentThread().getThreadGroup());
        }
}
class ThreadGroupDemo{

        public static void main(String[] args){

                Mythread obj=new Mythread("xyz");
		Mythread obj1=new Mythread("xyz");
		Mythread obj2=new Mythread("pqr");
		Mythread obj3=new Mythread();
		Mythread obj4=new Mythread();

                obj.start();
		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();

                System.out.println(Thread.currentThread().getThreadGroup());
        }
}
