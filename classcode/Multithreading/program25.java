
//Thread Pool
//Thread created when require as per task in newCachedThreadPool
import java.util.concurrent.*;

class Mythread implements Runnable{

	int num;
	Mythread(int num){

		this.num=num;
	}
	public void run(){

		System.out.println(Thread.currentThread()+"start Thread:"+num);
		dailytask();
		System.out.println(Thread.currentThread()+"End Thread:"+num);
	}
	void dailytask(){

		try{
			Thread.sleep(5000);

		}catch(InterruptedException ie){
		
		}
	}
}
class ThreadPoolDemo{

	public static void main(String[] args){

		ExecutorService ser=Executors.newCachedThreadPool();

		for(int i=1;i<=6;i++){

			Mythread obj=new Mythread(i);
			ser.execute(obj);
			
		}
		ser.shutdown();
	}
}
