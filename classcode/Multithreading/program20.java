
//Real time example


class Google extends Thread{

        Google(ThreadGroup tg,String str){

                super(tg,str);
        }
        public void run(){

                System.out.println(Thread.currentThread());

                System.out.println(Thread.currentThread().getThreadGroup());
        }

}

class ThreadGroupDemo{

        public static void main(String[] args){

                ThreadGroup pthreadGp=new ThreadGroup("Alphabet");
                ThreadGroup cthreadGp=new ThreadGroup(pthreadGp,"Google");

                Google obj1=new Google(cthreadGp,"Voice search");
                Google obj2=new Google(cthreadGp,"Google lens");
                Google obj3=new Google(cthreadGp,"Text search");

                obj1.start();
                obj2.start();
                obj3.start();
        }
}
