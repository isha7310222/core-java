
//Runnable interface

class Mythread implements Runnable{

	public void run(){

		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){

			System.out.println(ie.toString());

		}
	}
}
class ThreadGroupDemo{

	public static void main(String[] args)throws InterruptedException{

		ThreadGroup pthreadGP=new ThreadGroup("India");

		Mythread obj1=new Mythread();
		Mythread obj2=new Mythread();

		Thread t1=new Thread(pthreadGP,obj1,"Maha");
		Thread t2=new Thread(pthreadGP,obj2,"Goa");

		t1.start();
		t2.start();

		ThreadGroup cthreadGP=new ThreadGroup(pthreadGP,"Pakistan");

		Mythread obj3=new Mythread();
		Mythread obj4=new Mythread();

		Thread t3=new Thread(cthreadGP,obj3,"Karachi");
		Thread t4=new Thread(cthreadGP,obj4,"Lahore");

		t3.start();
		t4.start();

		ThreadGroup cthreadGP2=new ThreadGroup(pthreadGP,"Bangladesh");

		Mythread obj5=new Mythread();
		Mythread obj6=new Mythread();

		Thread t5=new Thread(cthreadGP2,obj5,"Dhaka");
		Thread t6=new Thread(cthreadGP2,obj6,"Mirpur");

		t5.start();
		t6.start();

		System.out.println(pthreadGP.activeCount());
		System.out.println(pthreadGP.activeGroupCount());

		System.out.println(cthreadGP.activeCount());
                System.out.println(cthreadGP.activeGroupCount());

		System.out.println(cthreadGP2.activeCount());
                System.out.println(cthreadGP2.activeGroupCount());


	}
}
