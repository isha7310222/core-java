
class MyThread extends Thread{

        public void run(){

                Thread t =Thread.currentThread();
                System.out.println(t.getPriority());
        }
}
class ThreadDemo{

        public static void main(String[] args){

                Thread t =Thread.currentThread();

                System.out.println(t.getPriority());

                MyThread obj1=new MyThread();
                obj1.start();

		try{
			t.setPriority(11);
		
		}catch(IllegalArgumentException obj){

			System.out.println("Priority must be in 0-10");
		}

                MyThread obj2=new MyThread();
                obj2.start();
        }
}
