
//changing thread group name

class Mythread extends Thread{

	Mythread(ThreadGroup tg,String str){

		super(tg,str);
	}
	public void run(){

		System.out.println(Thread.currentThread());

                System.out.println(Thread.currentThread().getThreadGroup());
	}

}

class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup pthreadGp=new ThreadGroup("C2W");

		Mythread obj1=new Mythread(pthreadGp,"C");
		Mythread obj2=new Mythread(pthreadGp,"java");
		Mythread obj3=new Mythread(pthreadGp,"Python");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}

