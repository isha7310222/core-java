class MyThread extends Thread{

        public void run(){

                Thread t =Thread.currentThread();
                System.out.println(t.getPriority());
        }
}
class ThreadDemo{

        public static void main(String[] args){

                Thread t =Thread.currentThread();

                System.out.println(t.getPriority());

                MyThread obj1=new MyThread();
                obj1.start();

		obj1.start();//IllegalThreadStateException Cannot be called already start method

                t.setPriority(7);

                MyThread obj2=new MyThread();
                obj2.start();
        }
}
