//Concurrency method in thread class
//yield method

class Mythread extends Thread{

	public void run(){

		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadYieldDemo{

	public static void main(String[] args){

		Mythread obj=new Mythread();
		obj.start();
		obj.yield();

		System.out.println(Thread.currentThread().getName());
	}
}
