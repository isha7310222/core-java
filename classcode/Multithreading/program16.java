
//change ThreadGroup name
//if we change threadname on setmethod it might possible that it will print different name as per priority

class Mythread extends Thread{

	public void run(){

		System.out.println(getName());
		System.out.println(Thread.currentThread());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
class ThreadGroupDemo{

	public static void main(String[] args)throws InterruptedException{

		Mythread obj=new Mythread();
		obj.start();
		
		Thread.sleep(100);
		obj.setName("Xyz");

		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
