
import java.util.concurrent.*;

class MyThread implements Runnable{

	int num;

	MyThread(int num){

		this.num=num;
	}
	public void run(){

		System.out.println(Thread.currentThread()+"Start thread"+num);
		dailytask();
		System.out.println(Thread.currentThread()+"End thread"+num);
	}
	void dailytask(){

		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){

		}
	}
}
class ThreadDemo{

	public static void main(String[] args){

		ThreadPoolExecutor tpe=(ThreadPoolExecutor)Executors.newFixedThreadPool(4);

		for(int i=1;i<=5;i++){

			Mythread obj=new Mythread(i);
			tpe.execute(obj);
		}
		tpe.shutdown();
	}
}

