//If we override start method then it will call local start method and thread created will not call run method remain of no use.

/*class mythread extends Thread{

        public void run(){

                System.out.println("In run");
                System.out.println(Thread.currentThread().getName());
        }
        public void start(){

                System.out.println("In my thread start");
                run();
        }
        */
class ThreadDemo{

        public static void main(String[] args){

                mythread obj=new mythread();
                obj.start();

                System.out.println(Thread.currentThread().getName());

        }
}
