

class Mythread extends Thread{

        Mythread(ThreadGroup tg,String str){

                super(tg,str);
        }
        public void run(){

                System.out.println(Thread.currentThread());

                System.out.println(Thread.currentThread().getThreadGroup());
        }

}

class ThreadGroupDemo{

        public static void main(String[] args){

                ThreadGroup pthreadGp=new ThreadGroup("C2W");
		ThreadGroup cthreadGp=new ThreadGroup(pthreadGp,"Incubator");

                Mythread obj1=new Mythread(cthreadGp,"flutter");
                Mythread obj2=new Mythread(cthreadGp,"Angular");
                Mythread obj3=new Mythread(cthreadGp,"ML");

                obj1.start();
                obj2.start();
                obj3.start();
        }
}
