//Active threads and active threadGroup
class Mythread extends Thread{

	Mythread(ThreadGroup tg,String str){

		super(tg,str);
	}
	public void run(){

		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){

			System.out.println("Exception:"+ie.toString());
		}
	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup pthreadGP=new ThreadGroup("India");

		Mythread t1=new Mythread(pthreadGP,"Maharashtra");
		Mythread t2=new Mythread(pthreadGP,"Goa");

		t1.start();
		t2.start();

		ThreadGroup cthreadGP=new ThreadGroup(pthreadGP,"Pakistan");
		
                Mythread t3=new Mythread(cthreadGP,"Lahore");
                Mythread t4=new Mythread(cthreadGP,"Karachi");

		t3.start();
		t4.start();

                ThreadGroup cthreadGP2=new ThreadGroup(pthreadGP,"Bangladesh");

                Mythread t5=new Mythread(cthreadGP2,"Dhaka");
                Mythread t6=new Mythread(cthreadGP2,"Mirpur");

                t5.start();
                t6.start();

		System.out.println(pthreadGP.activeCount());
		System.out.println(pthreadGP.activeGroupCount());
	}
}



