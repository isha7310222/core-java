//to check the capacity of StringBuffer

class StrBuf{

	public static void main(String[] args){

		StringBuffer sb=new StringBuffer();

		System.out.println(sb.capacity());

		sb.append("hello");

		System.out.println(sb.capacity());

		sb.append("java is programming language");

		System.out.println(sb.capacity());

		sb.append("String");

		System.out.println(sb.capacity());

	}
}
