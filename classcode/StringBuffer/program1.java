//String buffer ..if we concat or append string it make change in same object

class StringBufferDemo{

	public static void main(String[] args){

		StringBuffer str1=new StringBuffer("Isha");

		System.out.println(System.identityHashCode(str1));

		str1.append("Makhija");

		System.out.println(str1);

		System.out.println(System.identityHashCode(str1));
	}
}
