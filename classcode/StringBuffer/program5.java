//check capacity of String buffer
class StringBufferDemo{

	public static void main(String[] args){

		StringBuffer sb=new StringBuffer();

		System.out.println(sb.capacity());

		sb.append("Shashi");

		 System.out.println(System.identityHashCode(sb));

		System.out.println(sb.capacity());

		sb.append("Bagal");

		 System.out.println(System.identityHashCode(sb));

		System.out.println(sb.capacity());

		sb.append("Core2web");

		System.out.println(System.identityHashCode(sb));

		System.out.println(sb.capacity());

		sb.append("Welcome to java programming at Core2web Technologies");

		System.out.println(sb.capacity());

		System.out.println(System.identityHashCode(sb));

		sb.append("Welcome to java programming at Core2web Technologies");

                System.out.println(sb.capacity());

		 System.out.println(System.identityHashCode(sb));

	}
}
