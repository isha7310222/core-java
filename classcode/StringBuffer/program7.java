//append method trial n error

class AppendDemo{

	public static void main(String[] args){

		String str1="Shashi";

		String str2=new String("Bagal");

		StringBuffer str3=new StringBuffer("core2web");

		//String str=str1.append(str3);//error:cannot find symbol...append is method of StringBuffer class not string class
		
		StringBuffer str4=str3.append(str1);//StringBuffer can have parameter such as string or StringBuffer 

		//String str5=str1.concat(str3); //concat cannot have parameter except string

		StringBuffer str6=str3.append(str2);

		 System.out.println(str1);
		 System.out.println(str2);
		 System.out.println(str3);
		 System.out.println(str4);
		 System.out.println(str6);

		 System.out.println(System.identityHashCode(str1));
		 System.out.println(System.identityHashCode(str2));
		 System.out.println(System.identityHashCode(str3));
		 System.out.println(System.identityHashCode(str4));
		 System.out.println(System.identityHashCode(str6));
              

	}
}
/*str3 has to Core2web and str4 has to be core2webshashi and str6 has to be core2webBagal
 * but str3,str4,str6 have Core2WebShashiBagal as output coz append make change in same object i.e str3
 * thats why identityHashCode of str3,str4 and str6 are same*/
