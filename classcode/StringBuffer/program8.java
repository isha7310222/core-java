/*if appended string is not store in any object it will give reference of the appended string
 * but if concated string is not store in any object it will not give any reference to any object 
 * garbage collector will remove that object from heap*/

class SBDemo{

	public static void main(String[] args){

		String str1="Shashi";

		String str2=new String("Bagal");

		StringBuffer sb=new StringBuffer("Core2web");

		str1.concat(str2);

		sb.append(str2);

		System.out.println(str1);
		System.out.println(str2);
	        System.out.println(sb);

		 System.out.println(System.identityHashCode(str1));

		 System.out.println(System.identityHashCode(str2));

		 System.out.println(System.identityHashCode(sb));
	}
}
