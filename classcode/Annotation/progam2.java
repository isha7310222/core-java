
//Override Annotation

class Demo2{

	void m1(){

		System.out.println("Demo-m1");
	}
}
class DemoChild extends Demo2{

	@Override
	void m1(int x){

		System.out.println("Demochild-m1");
	}
}
