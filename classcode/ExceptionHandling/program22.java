//Userdefined Exception
//if data is less than 0 DataUnderFlowException if data is more than 100 DataOverFlowException

import java.util.*;

class DataUnderFlowException extends RuntimeException{

        DataUnderFlowException(String msg){

                super(msg);
        }
}
class DataOverFlowException extends RuntimeException{

        DataOverFlowException(String msg){

                super(msg);
        }
}
class Client{

        public static void main(String[] args){

                int arr[]=new int[5];

                Scanner sc=new Scanner(System.in);

                System.out.println("Enter integer value");
                System.out.println("Note:0< element <100");

                for(int i=0;i<arr.length;i++){
                        int data=sc.nextInt();

			try{
                        if (data<=0)
                
				throw new DataUnderFlowException("Data is less than zero");
		
			}catch(DataUnderFlowException duf){

				duf.printStackTrace();
			}
			try{

                        if(data>100)
                                
				throw new DataOverFlowException("Data is greater than 100");
	
			}catch(DataOverFlowException dof){

				dof.printStackTrace();
			}
                arr[i]=data;
                }
                System.out.println("Elements are:");
                for(int i=0;i<arr.length;i++){

                        System.out.println(arr[i]);
                }
        }
}
