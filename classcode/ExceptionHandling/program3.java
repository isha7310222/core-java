
//Default Exception Handler 
//ArithmeticException(Runtime)

class Demo{

	void m1(){

		System.out.println("In m1");
		System.out.println(10/0);

		m2();
	}
	void m2(){
		    System.out.println("In m2");
	}
	public static void main(String[] args){

		Demo obj=new Demo();
		obj.m1();
	}
}
/*Error:Exception in thread "main" java.lang.ArithmeticException: / by zero
        at Demo.m1(program3.java:11)
        at Demo.main(program3.java:21)      */
