//Runtime Exception can be used if it is never be thrown but Compiletime exception cannot be used if it will never be thrown

import java.io.*;
class InterruptedDemo{

        public static void main(String[] args){

                for(int i=0;i<9;i++){

                        System.out.println("Hello");
                        try{

                        Thread.sleep(500);
                       }catch(InterruptedException obj){

                                System.out.println("Thread is inturrupted");
                        }catch(RuntimeException obj1){
			}
			catch(IOException obj3){
			}//error: exception IOException is never thrown in body of corresponding try statement

                }
                        System.out.println("End main");


        }
}
