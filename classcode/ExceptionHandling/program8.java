
//IOException Stream Closed(Runtime)
import java.io.*;
class Input{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                String str=br.readLine();
                System.out.println(str);

                br.close();

                String str2="Default str";
                try{
                      str2=br.readLine();

                }catch(IOException obj){

                        System.out.println("Stream is closed you can't take input");

                }

                System.out.println(str2);

                System.out.println("End main");
        }
}

