
//Handle ArithmeticException by try catch

class Demo{

	public static void main(String[] args){

		System.out.println("Start Main");

		try{
			System.out.println(10/0);  //Risky Code

		}catch(ArithmeticException obj){

			System.out.println("Exception Occured");//Handling Code
		}

		 System.out.println("End Main");
	}
}
