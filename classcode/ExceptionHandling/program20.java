
//Real time Example

import java.util.*;

class PaymentModeException extends RuntimeException {

    PaymentModeException(String msg) {
        super(msg);
    }
}

class Shopping {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String selected_mode = null;

        System.out.println("Enter your payment mode");

        String mode = sc.next();

        if (mode.equals("Cash")) {
            throw new PaymentModeException("Cash payment not available");
        }

        selected_mode = mode;

        System.out.println(selected_mode + " PaymentMode accepted");
    }
}

