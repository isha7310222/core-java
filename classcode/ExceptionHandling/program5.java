//In which method exception occur that method and the method which is caller of that method both must throw the exception
import java.io.*;

class Demo{

	void getData()throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int data=Integer.parseInt(br.readLine());
	}
	public static void main(String[] args)throws IOException{
		Demo obj=new Demo();
		obj.getData();
	}
}
/*error: unreported exception IOException; must be caught or declared to be thrown
                int data=Integer.parseInt(br.readLine());
                                                     ^
						     */
