//Try must be with catch or finally
//catch must be with try
//error: 'try' without 'catch', 'finally' or resource declarations
// error: 'catch' without 'try'
class Demo{

        public static void main(String[] args){

                System.out.println("Start Main");

               /* try{
                        System.out.println(10/0);  //Risky Code

                }*/catch(ArithmeticException obj){

                        System.out.println("Exception Occured");//Handling Code
                }

                 System.out.println("End Main");
        }
}
