
import java.io.*;
import java.net.*;
import java.util.*;
class URLConnectionDemo{

	public static void main(String[] args)throws IOException{

		URL obj=new URL("https://youtube.com");

		URLConnection conn=obj.openConnection();

		System.out.println("Last modified =" + new Date(conn.getLastModified()));

	}
}

