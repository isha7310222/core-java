//If we close one object second object cannot take input as connection between pipe and keyboard will close
import java.io.*;
class IODemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br1=new BufferedReader(new InputStreamReader(System.in));
                BufferedReader br2=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter name1");
	        String str1=br1.readLine();
	System.out.println("String1="+str1);
	br1.close();
	System.out.println("Enter name2");
	String str2=br2.readLine();//Stream closed error
	System.out.println("String2="+str2);
	}
}

