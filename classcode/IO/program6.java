//Society name ,wing ,flat no
import java.io.*;
class Building{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter society name");
		String name=br.readLine();
		System.out.println("Enter wing name");
		char wing=(char)br.read();
		br.skip(1);
		System.out.println("Enter flat no");
		int flatNo=Integer.parseInt(br.readLine());
		System.out.println("Society name:"+name);
		System.out.println("Wing:"+wing);
		System.out.println("Flat:"+flatNo);
	}
}
