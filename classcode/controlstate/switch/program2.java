//With break
class SwitchDemo{
        public static void main(String[] args){
                int x=3;
                switch(x){
                        case 1:
                                System.out.println("one");
				break;
                        case 2:
                                 System.out.println("Two");
				 break;
                        case 3:
                                 System.out.println("Three");
				 break;
                        case 4:
                                 System.out.println("Four");
				 break;
                        default:
                                 System.out.println("Invalid");
				 break;
                }
               System.out.println("After switch");
        }
}
