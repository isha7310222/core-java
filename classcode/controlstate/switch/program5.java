class SwitchDemo{
        public static void main(String[] args){
                int ch=65;
                switch(ch){
                        case 'A':
                                System.out.println("Char A");
                                break;
                        case 65:
                                 System.out.println("var 65");//duplicate case label
                                 break;
                        default:
                                 System.out.println("Invalid");
                                 break;
                        case 66:
                                 System.out.println("var 66");
                                 break;
                        case 'B':
                                 System.out.println("char B");//duplicate case label
                                 break;
                }
               System.out.println("After switch");
        }
}
