//cannot use variables only constant values accepted
class SwitchDemo{
        public static void main(String[] args){
                int x=3;
		int a=1;
		int b=2;
                switch(x){
                        case a:
                                System.out.println("one");
                                break;
                        case b:
                                 System.out.println("Two");
                                 break;
                        default:
                                 System.out.println("Invalid");
                                 break;
                        case a+b:
                                 System.out.println("Three");//duplicate case label
                                 break;
                        case b+b:
                                 System.out.println("Four");
                                 break;
                }
               System.out.println("After switch");
        }
}
