
class SwitchDemo{
        public static void main(String[] args){
                int x=3;
                switch(x){
                        case 1:
                                System.out.println("one");
                                break;
                        case 2:
                                 System.out.println("Two");
                                 break;
                        default:
                                 System.out.println("Invalid");
                                 break;
                        case 1+1:
                                 System.out.println("Three");//duplicate case label
                                 break;
                        case 4:
                                 System.out.println("Four");
                                 break;
                }
               System.out.println("After switch");
        }
}
