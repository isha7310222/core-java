/* 1
 * 4  3
 * 16 5 36
 * 49 8 81 10 */
class Num{
        public static void main(String[] args){
                int N=1;
                for(int i=1;i<=4;i++){
                        for(int j=1;j<=i;j++){
				if(j%2==1){
					int sqr=N*N;
                                System.out.print(sqr +" ");
				}
				else{
					System.out.print(N+" ");
				}
				N++;
			}
			System.out.println();
		}
	}
}
