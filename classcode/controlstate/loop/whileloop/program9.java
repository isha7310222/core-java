// store multiplication of digits N=6531 o/p=6*5*3*1
class Mul{
	public static void main(String[] args){
		int N=6531;
		int mult=1;
		while(N!=0){
			int r=N%10;
			mult=mult*r;
			N=N/10;
		}
		System.out.println(mult);
	}
}
