
//cursor

import java.util.*;
class CursorDemo{

	public static void main(String[] args){

		//for each

		ArrayList al=new ArrayList();
		al.add(10);
		al.add(20.5);
		al.add(30.5f);
		al.add("C2W");

		for(var x:al){

			System.out.println(x.getClass().getName());
		}

		//iterator

		Iterator cursor=al.iterator();

		System.out.println(cursor.getClass().getName());

		while(cursor.hasNext()){

			if("C2W".equals(cursor.next()))
				cursor.remove();

		}
		System.out.println(al);

		//ListIterator

		ListIterator litr=al.listIterator();

		System.out.println(litr.getClass().getName());

		while(litr.hasPrevious()){

			System.out.println(litr.previous());
		}

		//Enumeration

		Vector v=new Vector();
		v.addElement(50);
		v.addElement(60);
		v.addElement(80);

		Enumeration enum1=v.elements();

		System.out.println(enum1.getClass().getName());

		while(enum1.hasMoreElements()){

			System.out.println(enum1.nextElement());
		}

	}
}
