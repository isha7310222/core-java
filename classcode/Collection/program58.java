
//Multithreading Revision

class mythread extends Thread{

        public void run(){

                Thread t=Thread.currentThread();

                System.out.println(t.getName());

                System.out.println(t.getPriority());

                t.setName("Isha");

                System.out.println(t.getName());
        }
}

class Demo extends Thread {

        public void run(){

                Thread t=Thread.currentThread();

                System.out.println(t.getName());

                System.out.println(t.getPriority());

                t.setName("Payal");

                System.out.println(t.getName());

                t.setPriority(7);

                mythread obj=new mythread();
                obj.start();

        }
}
class ThreadDemo{

        public static void main(String[] args){


                Thread t=Thread.currentThread();

                System.out.println(t.getName());

                System.out.println(t.getPriority());

                t.setName("Tanvi");

                System.out.println(t.getName());

                t.setPriority(6);

                Demo d1=new Demo();
                d1.start();
        }
}

