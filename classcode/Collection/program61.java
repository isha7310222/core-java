

//BlockingQueue

import java.util.concurrent.*;
import java.util.*;

class BlockingQueueDemo{

        public static void main(String [] args){

                BlockingQueue bQueue=new ArrayBlockingQueue(3);
	
                bQueue.offer(10);
                bQueue.offer(20);
                bQueue.offer(30);

                System.out.println(bQueue);

		try{

                bQueue.put(40);
		}catch(InterruptedException ie){

		
		}

                System.out.println(bQueue);
        }
}
