
//SortedMap Interface

import java.util.*;
class SortedMapDemo{

	public static void main(String[] args){

		SortedMap sm=new TreeMap();

                sm.put("Ind","India");
                sm.put("Pak","Pakistan");
                sm.put("SL","Srilanka");
                sm.put("Aus","Australia");
                sm.put("Ban","Bangladesh");

                System.out.println(sm);

		//<k,v> subMap(k,k)
		System.out.println(sm.subMap("Aus","Pak"));

		//<k,v> headMap(k)
		System.out.println(sm.headMap("Pak"));

		//<k,v> tailMap(k)
		System.out.println(sm.tailMap("Pak"));

		//k firstKey()
		System.out.println(sm.firstKey());

		//k lastKey()
		System.out.println(sm.lastKey());

		//set <k> keySet()
		System.out.println(sm.keySet());

		//collection <v> values()
		System.out.println(sm.values());

		//set <Map$Entry <k,v> >entrySet()
		System.out.println(sm.entrySet());


	}
}


