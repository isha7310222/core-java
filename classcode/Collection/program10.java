
//vector

import java.util.*;

class VectorDemo{

	public static void main(String[] args){

		Vector v=new Vector();
		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v.capacity());
		System.out.println(v);

		System.out.println(v.removeElement(2));

		v.setElementAt(50,1);

		System.out.println(v);

		System.out.println(v.isEmpty());

		v.ensureCapacity(25);
		System.out.println(v.capacity());




	
	}

}
