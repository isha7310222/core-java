
//Methods of HashMap

import java.util.*;

class HashMapMethods{

	public static void main(String[] args){

	HashMap hm=new HashMap();
	hm.put("Java",".java");
	hm.put("Python",".py");
	hm.put("Dart",".dart");

	System.out.println(hm);

	// V get (obj)
	
	System.out.println(hm.get("Python"));

	// V remove(obj)
	
	System.out.println(hm.remove("Java"));

	//set <k> keySet()...return keys
	
	System.out.println(hm.keySet());

	//collection <v> values()....return values
	
	System.out.println(hm.values());

	//set<java.util.Map$Entry <k,v>> entrySet()....convert into set
	
	System.out.println(hm.entrySet());
	}
}

