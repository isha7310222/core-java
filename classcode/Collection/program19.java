
import java.util.*;

class CricPlayer implements Comparable {

        int jerNo=0;
        String Name=null;


        CricPlayer(int jerNo,String Name){

		this.jerNo=jerNo;
                this.Name=Name;

        }
	public int compareTo(Object obj){

		CricPlayer obj1=(CricPlayer)obj;
		HashSet hs1=new HashSet();
		hs1.add(obj1);
	        return this.Name.compareTo(obj1.Name);
	}

	public String toString(){

	          return jerNo+":"+Name;
	}

}
class HashSetDemo{

        public static void main(String[] args){

                HashSet hs=new HashSet();

                hs.add(new CricPlayer(18,"Virat"));

                hs.add(new CricPlayer(7,"MSD"));

                hs.add(new CricPlayer(45,"Rohit"));

                hs.add(new CricPlayer(7,"MSD"));

                System.out.println(hs);
        }
}
