
//using var in for each loop

import java.util.*;
class ArrayListDemo{

        public static void main(String[] args){

                ArrayList al=new ArrayList();

                al.add(10);
                al.add("Isha");
                al.add(new Integer(40));

                for(var obj:al){

                        System.out.println(obj);
                }
        }
}
