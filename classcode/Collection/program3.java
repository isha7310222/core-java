
//user defined class pass as a object and print using for loop
//it gives address do not override to string method to print data

import java.util.*;

class ItCompany{

	int empNo=0;
	String cmpName=null;

	ItCompany(int empNo,String cmpName){

		this.empNo=empNo;
		this.cmpName=cmpName;

	}
}
class ArrayListDemo{

	public static void main(String[] args){

		ArrayList al=new ArrayList();

		al.add(10);
		al.add("Isha");
		al.add(new ItCompany(15,"CityFlare"));

		for(int i=0;i<al.size();i++){

			System.out.println(al);
		}
	}
}

