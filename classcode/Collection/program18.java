
//userdefined class pass as a object does not handle duplicate objects

import java.util.*;

class CricPlayer{

	int jerNo=0;
	String Name=null;

	CricPlayer(int jerNo,String Name){

		this.jerNo=jerNo;
		this.Name=Name;
	}
}
class HashSetDemo{

	public static void main(String[] args){

		HashSet hs=new HashSet();

		hs.add(new CricPlayer(18,"Virat"));

		hs.add(new CricPlayer(7,"MSD"));

		hs.add(new CricPlayer(45,"Rohit"));

		hs.add(new CricPlayer(7,"MSD"));

		System.out.println(hs);
	}
}
