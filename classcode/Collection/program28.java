
// methods of sorted set

import java.util.*;

class SortedSetDemo{

	public static void main(String[] args){

		SortedSet ss=new TreeSet();
		ss.add("Isha");
		ss.add("Payal");
		ss.add("Ruchita");
		ss.add("Tanvi");
		ss.add("Shital");

		System.out.println(ss);

		//subSet(E,E)...return elements range between alphabetically including that
		System.out.println(ss.subSet("Isha","Shital"));

		//headSet(E)...return previous elements 
		System.out.println(ss.headSet("Isha"));
		System.out.println(ss.headSet("Ruchita"));

		//tailSet(E)...return after elemets including that
		 System.out.println(ss.tailSet("Payal"));

		 //last()
		  System.out.println(ss.last());

		  //first()
		  System.out.println(ss.first());
	}
}
