
//BlockingQueue

import java.util.concurrent.*;
import java.util.*;

class PriorityBlockingQueueDemo{

        public static void main(String [] args)throws InterruptedException{

                BlockingQueue bQueue=new PriorityBlockingQueue(3);
	
                bQueue.put(10);
                bQueue.put(20);
                bQueue.put(30);

                System.out.println(bQueue);

		bQueue.offer(40,5,TimeUnit.SECONDS);

		System.out.println(bQueue);

		System.out.println(bQueue.remainingCapacity());

		Iterator itr=bQueue.iterator();

		while(itr.hasNext()){

			System.out.println(itr.next());
		}
	
		System.out.println(bQueue.poll(4,TimeUnit.SECONDS));

		System.out.println(bQueue);

                bQueue.take();

                System.out.println(bQueue);

                ArrayList al=new ArrayList();

                System.out.println("ArrayList"+al);

                bQueue.drainTo(al);

                System.out.println("ArrayList"+al);

                System.out.println(bQueue);


	}
}
