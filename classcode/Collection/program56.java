
//Deque

import java.util.*;
class DequeDemo{

	public static void main(String[] args){

		Deque obj=new ArrayDeque();

		obj.offer(10);
		obj.offer(20);
		obj.offer(40);
		obj.offer(50);

		System.out.println(obj);

		//offerFirst()
		obj.offerFirst(5);

		//offerLast()
		obj.offerLast(60);

		System.out.println(obj);

		//pollFirst()
		obj.pollFirst();

		//pollLast()
		obj.pollLast();

		System.out.println(obj);
		//peekFirst()
		System.out.println(obj.peekFirst());

		//peekLast()
		     System.out.println(obj.peekLast());

		//iterator()

		     Iterator itr=obj.iterator();
		     while(itr.hasNext()){

			     System.out.println(itr.next());
		     }

		//descendingIterator
	           
		     Iterator itr2=obj.descendingIterator();
                     while(itr2.hasNext()){

                             System.out.println(itr2.next());
                     }     
	}
}


