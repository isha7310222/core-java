//Hashmap dominates GC
import java.util.*;
class Demo{

        String str;
        Demo(String str){

                this.str=str;
        }
        public String toString(){

                return str;
        }
        public void finalize(){//before removing object call finalize

                System.out.println("Notify");
        }
}
class GCDemo{

        public static void main(String[] args){

                Demo obj1=new Demo("C2W");
                Demo obj2=new Demo("Biencaps");
                Demo obj3=new Demo("Incubator");

                System.out.println(obj1);
                System.out.println(obj2);
                System.out.println(obj3);

		HashMap hm=new HashMap();
		hm.put(obj1,2016);
		hm.put(obj2,2019);
		hm.put(obj3,2023);

		System.out.println(hm);

		obj1=null;
		obj2=null;

		System.out.println(hm);
                
		System.gc();//hashmap dominates GC

                System.out.println("In main");

        }
}
