
//Navigable interface methods

import java.util.*;

class NavigableDemo{

	public static void main(String[] args){

		NavigableSet ns=new TreeSet();

		ns.add("Isha");
		ns.add("Payal");
		ns.add("Ruchita");
		ns.add("Shital");
		ns.add("Tanvi");

		System.out.println(ns);

		//public abstract E lower(E)..(<)
		System.out.println(ns.lower("Shital"));

		//public abstract E floor(E)..(<=) 
		//It returns the greatest element in this set less than or equal to the given element, or null if there is no such element.
		System.out.println(ns.floor("Shital"));

		//public abstract ceiling(E)...(>=)
		//It returns the least element in this set greater than or equal to the given element, or null if there is no such element.
		System.out.println(ns.ceiling("Ruchita"));

		//public abstract E higher(E)

		System.out.println(ns.higher("Isha"));

		//public abstract E pollFirst()

		System.out.println(ns.pollFirst());

		//public abstract E pollLast()
		System.out.println(ns.pollLast());

		// public abstract java.util.Iterator<E> iterator();
		Iterator itr=ns.iterator();
		while(itr.hasNext()){

			System.out.println(itr.next());
		}

             	// public abstract java.util.NavigableSet<E> descendingSet();
		System.out.println(ns.descendingSet());
             
	      
		// public abstract java.util.Iterator<E> descendingIterator();
		Iterator itr1=ns.descendingIterator();
		while(itr1.hasNext()){

			System.out.println(itr1.next());
		}
        
	 
		// public abstract java.util.NavigableSet<E> subSet(E, boolean, E, boolean);
		System.out.println(ns.subSet("Payal",true,"Shital",false));//true for include ..false for exclude
        
	 
		// public abstract java.util.NavigableSet<E> headSet(E, boolean);
		System.out.println(ns.headSet("Ruchita",true));
        
	 
		// public abstract java.util.NavigableSet<E> tailSet(E, boolean);
		System.out.println(ns.tailSet("Payal",true));
        
	 
		// public abstract java.util.SortedSet<E> subSet(E, E);
		System.out.println(ns.subSet("Payal","Shital"));
        
	 
		// public abstract java.util.SortedSet<E> headSet(E);
		System.out.println(ns.headSet("Ruchita"));
        
	 
		// public abstract java.util.SortedSet<E> tailSet(E);
		System.out.println(ns.tailSet("Shital"));



	}
}
