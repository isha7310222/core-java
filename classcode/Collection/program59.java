
//Comparator in HashMap
import java.util.*;

class Demo{

	String Name;
	String CompName;

	Demo(String Name,String comName){

		this.Name=Name;
		this.CompName=CompName;
	}

	public String toString(){

		return "{"+Name+" "+CompName+"}";
	}

}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){

		return ((Demo)obj1).Name.compareTo(((Demo)obj2).Name);
	}
}
class HashMapDemo{

        public static void main(String[] args){

                HashMap hm=new HashMap();
                hm.(new Demo("Kanha","Infosys"));
                hm.put(new Demo("Ashish","Barclays"));
                hm.put(new Demo("Kanha","Carpro"));
                hm.put(new Demo("Rahul","BMC"));

		ArrayList ar=new ArrayList(hm);

		Collections.sort(ar,new SortByName());

                System.out.println(ar);

        }
}

