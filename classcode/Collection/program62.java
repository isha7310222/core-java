
//BlockingQueue

import java.util.concurrent.*;
import java.util.*;

class Demo extends Thread{

	BlockingQueue bQueue=null;

	Demo(BlockingQueue bQueue){

		this.bQueue=bQueue;
	}
	public void run(){	

	 try{
		 notify();
		 bQueue.put(40);
		 

	 }catch(InterruptedException ie){


	    }
	}
}
class BlockingQueueDemo{

        public static void main(String [] args){

                BlockingQueue bQueue=new ArrayBlockingQueue(3);
	
                bQueue.offer(10);
                bQueue.offer(20);
                bQueue.offer(30);

		System.out.println(bQueue);

		Demo obj=new Demo(bQueue);
		obj.start();

                System.out.println(bQueue);
        }
}
