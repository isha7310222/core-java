
import java.util.*;

class IteratorDemo{

	public static void main(String[] args){

		ArrayList al=new ArrayList();

		al.add("Isha");
		al.add("Mahima");
		al.add("Ayush");

		Iterator itr=al.iterator();

		while(itr.hasNext()){

			if("Mahima".equals(itr.next()))
				itr.remove();

			System.out.println(itr.next());
		}
		System.out.println(al);
	}
}
