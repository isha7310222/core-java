
//comparator

import java.util.*;
class Employee{

	String empName=null;
	int sal=0;

	Employee(String empName,int sal){

		this.empName=empName;
		this.sal=sal;
	}
	public String toString(){

		return "{"+empName+": "+sal+"}";
	}
}
class SortByName implements Comparator<Employee>{

	public int compare(Employee obj1,Employee obj2){

		return obj1.empName.compareTo(obj2.empName);
	}
}
class SortBySal implements Comparator<Employee>{

	public int compare(Employee obj1,Employee obj2){

		return (int)(obj1.sal) -(obj2.sal);
	}
}
class ListSortDemo{

	public static void main(String[] args){

		ArrayList<Employee> al=new ArrayList<Employee>();

		al.add(new Employee("Kanha",200000));
		al.add(new Employee("Badhe",300000));
		al.add(new Employee("Ashish",250000));
                al.add(new Employee("Rahul",320000));
		al.add(new Employee("Shashi",3600000));

		System.out.println(al);

		Collections.sort(al,new SortBySal());
		System.out.println(al);

		Collections.sort(al,new SortByName());
		System.out.println(al);

	}
}
