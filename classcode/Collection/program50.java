
//Naviable Map ..

import java.util.*;

class NavigableMapDemo{

        public static void main(String[] args){

                NavigableMap nm=new TreeMap();

                nm.put("Isha",10);
                nm.put("Payal",20);
                nm.put("Ruchita",12);
                nm.put("Shital",50);
                nm.put("Tanvi",32);

                System.out.println(nm);

                //public abstract java.util.Map$Entry<K, V> lowerEntry(K);..(<)
                System.out.println(nm.lowerEntry("Shital"));

		//  public abstract K lowerKey(K);
		 System.out.println(nm.lowerKey("Shital"));

                //public abstract java.util.Map$Entry<K, V> floorEntry(K);...(<=)
                //It returns the greatest element in this set less than or equal to the given element, or null if there is no such element.
                System.out.println(nm.floorEntry("Shital"));


		// public abstract K floorKey(K);
		 System.out.println(nm.floorKey("Shital"));


                // public abstract java.util.Map$Entry<K, V> ceilingEntry(K);...(>=)
                //It returns the least element in this set greater than or equal to the given element, or null if there is no such element.
                System.out.println(nm.ceilingEntry("Ruchita"));

	
		// public abstract K ceilingKey(K);
		System.out.println(nm.ceilingKey("Ruchita"));
		

                //  public abstract java.util.Map$Entry<K, V> higherEntry(K);

                System.out.println(nm.higherEntry("Isha"));


		//public abstract K higherKey(K);

                System.out.println(nm.higherKey("Isha"));


                //public abstract java.util.Map$Entry<K, V> firstEntry();

                System.out.println(nm.firstEntry());
		

		//public abstract java.util.Map$Entry<K, V> lastEntry();

		  System.out.println(nm.lastEntry());
		

		//  public abstract java.util.Map$Entry<K, V> pollFirstEntry();

                System.out.println(nm.pollFirstEntry());

                //  public abstract java.util.Map$Entry<K, V> polllastEntry();
                System.out.println(nm.pollLastEntry());


                //  public abstract java.util.NavigableMap<K, V> descendingMap();

                System.out.println(nm.descendingMap());	


		// public abstract java.util.NavigableMap<K, V> subMap(K, boolean, K, boolean);
                System.out.println(nm.subMap("Payal",true,"Shital",false));//true for include ..false for exclude


		// public abstract java.util.NavigableMap<K, V> headMap(K, boolean);
                System.out.println(nm.headMap("Ruchita",true));


		//  public abstract java.util.NavigableMap<K, V> tailMap(K, boolean);
                System.out.println(nm.tailMap("Payal",true));


		//  public abstract java.util.SortedMap<K, V> subMap(K, K);
                System.out.println(nm.subMap("Payal","Shital"));


		//  public abstract java.util.SortedMap<K, V> headMap(K);
                System.out.println(nm.headMap("Ruchita"));


		//  public abstract java.util.SortedMap<K, V> tailMap(K);
                System.out.println(nm.tailMap("Shital"));



        }
}
