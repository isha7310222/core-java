//LinkedHashMap
//preserve insertion order

import java.util.*;

class HashMapDemo{

	public static void main(String[] args){

		LinkedHashMap hm=new LinkedHashMap();

		hm.put("Badhe","Infosys");
		hm.put("Kanha","Barclays");
		hm.put("Rahul","BMC");

		System.out.println(hm);
	}
}
