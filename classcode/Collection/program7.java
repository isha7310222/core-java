//LinkedList

import java.util.*;

class LinkedData{

	String name=null;
	int age=0;

	LinkedData(String name,int age){

		this.name=name;
		this.age=age;
	}
	public String toString(){

		return name+":"+age;
	}
}
class LinkedListDemo{

	public static void main(String[] args){

		LinkedList ll=new LinkedList();
		ll.add(20);

		// void addFirst()
		ll.addFirst(10);
		System.out.println(ll);

		// void addLast(E)
		ll.addLast(30);
                System.out.println(ll);
		
		//E getFirst()
		System.out.println(ll.getFirst());
	

		//E getLast()
		System.out.println(ll.getLast());

		//E removeFisrt()
		System.out.println(ll.removeFirst());

		//E removeLast()
		System.out.println(ll.removeLast());

		ll.addFirst(50);
		ll.addLast(80);

		//at specific index but do not use this
		ll.add(2,35);
		System.out.println(ll);

		ll.add(new LinkedData("Isha",21));

		System.out.println(ll);

		//LinkedList$Node node (int)

	
		Systwm.out.println(ll.node(2));


	}
}
