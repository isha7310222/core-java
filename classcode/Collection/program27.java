//Real time example

import java.util.*;

class Theatre{

        String seatType=null;
        float price=0.0f;

        Theatre(String seatType,float price){

                this.seatType=seatType;
                this.price=price;
        }

        public String toString(){

                return "{"+seatType+": "+price+"}";
        }
}
class SortByPrice implements Comparator{

        public int compare(Object obj1,Object obj2){

                return (int)((((Theatre)obj1).price) -(((Theatre)obj2).price));
        }
}
class TicketSorting{

        public static void main(String[] args){

                ArrayList al=new ArrayList();

		al.add(new Theatre("Executive",300.0f));
                al.add(new Theatre("Normal",250.0f));
                al.add(new Theatre("premium",350.0f));

                System.out.println(al);

                Collections.sort(al,new SortByPrice());

                System.out.println(al);

        }
}
