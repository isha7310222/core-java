
//BlockingQueue

import java.util.concurrent.*;
import java.util.*;

class Demo{

        String str;
        Demo(String str){

                this.str=str;
        }

        public String toString(){

                return str;
        }
}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){

		return (((Demo)obj1).str).compareTo(((Demo)obj2).str);
	}
}
class PriorityBlockingQueueDemo{

        public static void main(String [] args)throws InterruptedException{

                BlockingQueue bQueue=new PriorityBlockingQueue(3,new SortByName());

                bQueue.put(new Demo("Rahul"));
                bQueue.put(new Demo("Kanha"));
                bQueue.put(new Demo("Ashish"));

                bQueue.put(new Demo("Badhe"));
                System.out.println(bQueue);


        }
}
