
//multithreading in vector

import java.util.*;

class Mythread extends Thread{

	Vector v=null;

	Mythread(Vector v){

		this.v=v;
	}

	public void run(){

		System.out.println(v.hashCode());

		v.addElement(60);
		v.addElement(70);
		v.addElement(80);

		System.out.println(v);
	}

}

class VectorDemo1{

	public static void main(String[] args){


	Vector v=new Vector();

	v.addElement(10);
	v.addElement(20);
	v.addElement(30);
	v.addElement(40);

	System.out.println(v);

	Mythread obj=new Mythread(v);
	obj.start();

	System.out.println(v.removeElement(30));

	System.out.println(v.capacity());
	}
}

