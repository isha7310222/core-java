
//BlockingQueue

import java.util.concurrent.*;
import java.util.*;

class Demo implements Comparable{

	String str;
	Demo(String str){

		this.str=str;
	}
	public int compareTo(Object obj){

		return this.str.compareTo(((Demo)obj).str);
	}

	public String toString(){

		return str;
	}
}

class PriorityBlockingQueueDemo{

        public static void main(String [] args)throws InterruptedException{

                BlockingQueue bQueue=new PriorityBlockingQueue(3);
	
                bQueue.put(new Demo("Rahul"));
                bQueue.put(new Demo("Kanha"));
                bQueue.put(new Demo("Ashish"));

		bQueue.put(new Demo("Badhe"));
                System.out.println(bQueue);


	}
}
