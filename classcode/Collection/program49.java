
import java.util.*;
import java.io.*;

class PropertiesDemo{

	public static void main(String[] args)throws IOException{

		Properties obj=new Properties();

		FileInputStream inobj=new FileInputStream("friends.properties");

		obj.load(inobj);

		String name=obj.getProperty("Payal");

		System.out.println(name);

		obj.setProperty("Isha","eQ technologic");

		FileOutputStream outobj=new FileOutputStream("friends.properties");

		obj.store(outobj,"updated by Isha");
	}
}


