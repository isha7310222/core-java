
//Queue interface

import java.util.*;

class QueueDemo{

	public static void main(String[] args){

		Queue que=new LinkedList();

		que.offer(10);
		que.offer(20);
		que.offer(50);
		que.offer(30);
		que.offer(40);

		que.add(60);
		que.add(70);

		System.out.println(que);

		//E poll()..remove first element
		System.out.println(que.poll());

                System.out.println(que);

		//E peek()..return first element
		System.out.println(que.peek());

		System.out.println(que);

                Queue que1=new LinkedList();

		//remove()..remove first element
		try{
		System.out.println(que1.remove());//if queue empty thorows exception
		}catch(NoSuchElementException nse){

		}
                System.out.println(que1);
                //E element()..return first element 
		try{
		System.out.println(que1.element());//if queue empty thorows exception
		}catch(NoSuchElementException obj){

		}
                System.out.println(que1);
	}
}
