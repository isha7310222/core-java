
//comparator

import java.util.*;

class Movie{

	String movName=null;
	double totcoll=0.0;
	float ImdbRating=0.0f;

	Movie(String movName,double totcoll,float ImdbRating){

		this.movName=movName;
		this.totcoll=totcoll;
		this.ImdbRating=ImdbRating;
	}

	public String toString(){

		return "{"+movName+","+totcoll+","+ImdbRating+"}";
	}
}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){

		return ((((Movie)obj1).movName).compareTo(((Movie)obj2).movName));
	}
}
class SortByColl implements Comparator{

	public int compare(Object obj1,Object obj2){

		return (int)((((Movie)obj1).totcoll) - (((Movie)obj2).totcoll));
	}
}
class SortByImdb implements Comparator{

	public int compare(Object obj1,Object obj2){

		return (int)((((Movie)obj1).ImdbRating)  -(((Movie)obj2).ImdbRating));
	}
}
class UserListSort{

	public static void main(String[] args){

		ArrayList al=new ArrayList();

		al.add(new Movie("RHTDM",200.0,9.0f));
	        al.add(new Movie("Ved",250.0,8.0f));
                al.add(new Movie("Gadar2",100.0,7.0f));
		al.add(new Movie("Omg2",300.0,6.0f));

		System.out.println(al);

		Collections.sort(al,new SortByName());

		System.out.println(al);

		Collections.sort(al,new SortByColl());

		System.out.println(al);

		Collections.sort(al,new SortByImdb());

		System.out.println(al);
	}
}
