
//comparable with PriorityQueue

import java.util.*;

class Project implements Comparable{

	String projName;
	int teamSize;
	int duration;

	Project(String projName,int teamSize,int duration){

		this.projName=projName;
		this.teamSize=teamSize;
		this.duration=duration;
	}
	public int compareTo(Object obj){

		return (this.projName).compareTo(((Project)obj).projName);
	}
	public String toString(){

		return "{"+projName+", "+teamSize+", "+duration+"}";
	}
}
class PriorityDemo{

	public static void main(String[] args){

		PriorityQueue pq=new PriorityQueue();

		pq.offer(new Project("Java",15,20));
		pq.offer(new Project("C",17,30));
		pq.offer(new Project("CPP",5,35));
	        pq.offer(new Project("DSA",8,20));

		System.out.println(pq);
	}
}
