//Comparator 
import java.util.*;

class Project{ 

        String projName;
        int teamSize;
        int duration;

        Project(String projName,int teamSize,int duration){

                this.projName=projName;
                this.teamSize=teamSize;
                this.duration=duration;
        }
       
        public String toString(){

                return "{"+projName+", "+teamSize+", "+duration+"}";
        }
}
class SortByDuration implements Comparator{

	public int compare(Object obj1,Object obj2){

		return (int)(((Project)obj1).duration)-(((Project)obj2).duration);
	}
}
class PriorityDemo{

        public static void main(String[] args){

                PriorityQueue pq=new PriorityQueue(new SortByDuration());
		
                pq.offer(new Project("Java",15,20));
                pq.offer(new Project("C",17,30));
                pq.offer(new Project("CPP",5,35));
                pq.offer(new Project("DSA",8,20));
/*
		ArrayList al=new ArrayList(pq);

		Collections.sort(al,new SortByDuration());
*/
 		System.out.println(pq);
        }
}
