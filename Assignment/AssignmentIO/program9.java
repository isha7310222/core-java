/*WAP to take no as input & print the addition of factorial of each digit from that no 
 * I/P = 1234
 * O/p = 33 */
import java.io.*;
class Factorial{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int N=Integer.parseInt(br.readLine());
		int sum=0;
		while(N!=0){
			int r=N%10;
			int fact=1;
			for(int i=1;i<=r;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			N=N/10;
		}
		System.out.println(sum);
	}
}
