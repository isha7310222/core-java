/* o
 * 14 13
 * L  K  J
 * 9  8  7  6
 * E  D  C  B  A*/
import java.io.*;
class AlphaNum{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int n=(row*(row+1))/2;
		char ch=(char)(64+n);
		if(row<=7){

		for(int i=1;i<=n;i++){

			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print(n+" ");
				}
				else{
					System.out.print(ch+"  ");
				}
				n--;
				ch--;
			}
			System.out.println();
		}
		
		}
		else{
			System.out.println("Row should be less than 7");
		}
	}
}
				
