//WAP to print all even numbers in reverse order and odd no in standard way within a range take the start & end from user.
import java.io.*;
class Even{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int start=Integer.parseInt(br.readLine());
		int end= Integer.parseInt(br.readLine());
		for(int i=end;i>=start;i--){
                            if(i%2==0){
					System.out.print(i+" ");
				}
			}
		System.out.println();
		for(int i=start;i<=end;i++){
			if(i%2==1){
					System.out.print(i+" ");
				}
		}
			
		
	}
}
