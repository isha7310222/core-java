/* take row input from user
 * 0
 * 1 1
 * 2  3  5
 * 8 13 21 34 fibonacci series*/
import java.io.*;
class Fibonacci{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int a=0;
		int b=1;
		int c;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				c=a;
				System.out.print(c+" ");
				a=b;
				b=c+b;
			}
		System.out.println();
		}
	}
}

