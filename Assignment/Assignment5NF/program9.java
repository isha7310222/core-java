/*1
 *8 9
 27 16 125
 64 25 216 49*/
class Cube{
	public static void main(String[] args){
		for(int i=1;i<=4;i++){
			int N=i;
			for(int j=1;j<=i;j++){
				if(j%2==1){
					int cube=N*N*N;
					System.out.print(cube+" ");
				}
				else{
					int sqr=N*N;
					System.out.print(sqr+" ");
				}
				N++;
			}
			System.out.println();
		}
	}
}
