//write a program to print the square of even digits of the giv no N=942111423 o/p=4,16,4,16
class Even{
	public static void main(String[] args){
		int N=942111423;
		while(N!=0){
			int r=N%10;
			N=N/10;
			if(r%2==0){
				int squr=r*r;
				System.out.println(squr);
			}
		}
	}
}
