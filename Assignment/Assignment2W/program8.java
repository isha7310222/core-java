//write a program to printvthe countdown of days to submit the assignment N=day=7..o/p=7 days remaining..6 days remaining..1 days remaining..0 days assignment is overdue
class Days{
	public static void main(String[] args){
		int day=7;
		while(day>=0){
			if(day>0){
				System.out.println(day+"days remaining");
			}
			else{
				System.out.println(day+"days assignment is overdue");
			}
			day--;
		}
	}
}
