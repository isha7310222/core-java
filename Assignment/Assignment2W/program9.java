//write the program to reverse the giv no N=942111423...o/p=324111249
class Reverse{
	public static void main(String[] args){
		int N=942111423;
		int rev=0;
		while(N!=0){
			int r=N%10;
			rev=rev*10+r;
			N=N/10;
		}
		System.out.println(rev);
	}
}
