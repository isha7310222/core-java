// write a program to count the odd digits of giv no N=942111423....count of odd digits=5
class Odd{
	public static void main(String[] args){
		int N=942111423;
		int count=0;
		while(N!=0){
			int r=N%10;
			N=N/10;
			if(r%2!=0){
				count++;
			}
		}
		 System.out.println(count);
	}
}
