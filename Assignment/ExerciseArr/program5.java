//WAP to take 10  integer elements from user and print elements divisible by 5
import java.io.*;
class DemoArray5{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                
		int arr[]=new int[10];
                System.out.println("Enter 10 array elements");
                
		for(int i=0;i<arr.length;i++){
                
	       		arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0;i<arr.length;i++){
                
	       		if(arr[i]%5==0){
               
	      			System.out.println("Elements divisible by 5="+arr[i]);
                         }
                }

        }
}
