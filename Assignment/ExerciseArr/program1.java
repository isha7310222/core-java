//WAP to take size of array from user and take integer elements from user print sumof odd elments only
import java.io.*;
class DemoArray1{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                 
		System.out.println("Enter array size");		
	       	int size=Integer.parseInt(br.readLine());
               
	       	int arr[]=new int[size];
		int sum=0;
	
		System.out.println("Enter array elements");
               
	       	for(int i=0;i<arr.length;i++){
        
       			arr[i]=Integer.parseInt(br.readLine());
			
		       	if(arr[i]%2==1){
				 sum=sum+arr[i];
			 }
		}
		System.out.println("Sum of odd elements="+sum);
        
        }
}
