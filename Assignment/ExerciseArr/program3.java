//WAP to take size of array from user and take integer elements from user and print product of odd index only
import java.io.*;
class DemoArray3{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                 
		System.out.println("Enter array size");
                
	       	int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];
              	int product=1;
              
	      	System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){
              
	     		arr[i]=Integer.parseInt(br.readLine());
             
	    		if(i%2==1){
                                 product=product*arr[i];
                         }
                }
                System.out.println("product of odd index elements="+product);

        }
}
