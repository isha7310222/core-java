//WAP to take 7 character as input print only vowels from array
import java.io.*;
class DemoArray4{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter 7 characters ");
       		char arr[]=new char[7];
       
      		for(int i=0;i<arr.length;i++){
      
     			arr[i]=(char)(br.read());
			 br.skip(1);
		}
	
		for(int i=0;i<arr.length;i++){
                 
		   if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
                   
	      		   System.out.println("Vowels="+arr[i]);
                         }
                }

        }
}
