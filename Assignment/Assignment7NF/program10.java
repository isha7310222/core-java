/*WAP to take range as input from the user and print Armstrong numbers.
 *Input:Enter start:1
        Enter end:1650
  Output:Armstrong numbers between 1 and 1650
        1 2 3 4 5 6 7 8 9 153 370 371 407 1634
*/
import java.io.*;
class ArmstrongNum{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter end");
		int end=Integer.parseInt(br.readLine());

		System.out.println("Armstrong numbers between"+start+" and "+end);

		for(int i=start;i<=end;i++){

			int temp1=i;
			int count=0;
			int temp2=i;
			int sum=0;
			while(temp1!=0){
				temp1=temp1/10;
				count++;
			}
			while(temp2!=0){

                                int mul=1;
				int r=temp2%10;
				temp2=temp2/10;

			for(int j=1;j<=count;j++){

				 mul=mul*r;
			}
			sum=sum+mul;
			}
		if(sum==i){

			System.out.print(i+" ");
		}
	}
	
	}
	
}






