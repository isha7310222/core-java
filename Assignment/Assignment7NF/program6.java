/*WAP to take 5 numbers as input from the user and print the count of digits in those numbers.
 *Input:Enter 5 numbers:
 5
 The digit count in 5 is 1.
 25
 The digit count in 25 is 2.
 */

import java.io.*;
class CountDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int start[]=new int[5];

                System.out.println("Enter 5 elemnts");

		for(int i=0;i<5;i++){

		 start[i]=Integer.parseInt(br.readLine());

		int count=0;
		int temp=start[i];

		while(start[i]!=0){
			
			start[i]=start[i]/10;
			count++;
		}
		System.out.println("The digit count in "+temp+" is "+count);
		
        }
		
                
          
        }
}

