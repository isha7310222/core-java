/*WAP to take range input from user and print composite numbers
 * Enter start:1
 * Enter end:20
 * o/p:Composite numbers between 1 to 20 
 * 4,6,8,10,12,14,15,16,18,20 */
import java.io.*;

class Composite{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter starting range");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter ending range");
		int end=Integer.parseInt(br.readLine());

		System.out.println("Composite numbers between "+start+" to "+end);

		for(int i=start;i<=end;i++){
		
			int count=0;

			for(int j=1;j<i;j++){

				if(i==1 ||i==2){
					
				       break;
				}
                            
				if(i%j==0){
					count++;
				}
			        if(count>i=2){

				System.out.print(i+" ");
				break;
			}
		}
		
	}
	}
}


