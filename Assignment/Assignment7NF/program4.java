/*WAP to take range as input from the user and print perfect cubes between that range
 *Input:Enter start:1
        Enter end:100
  Output:perfect cubes between 1 to 100
 1 8 27 64
*/
import java.io.*;
class PerfectCubeDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter start");
                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter end");
                int end=Integer.parseInt(br.readLine());

                for(int i=start;i<=end;i++){

                        for(int j=1;j*j*j<=i;j++){

                                if(j*j*j==i){
                        System.out.println(i+" ");
                        }

                }
        }
}
}
