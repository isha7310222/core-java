/*WAP to print the no divisible by 5 & if no is even also print the count of even no's
 enter lower limit:1
 enter upper limit:50
 o/p:10,20,30,40,50
 count:5 */

import java.io.*;

class Divisible{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter lower limit");
		int lower=Integer.parseInt(br.readLine());

		System.out.println("Enter upper limit");
		int upper=Integer.parseInt(br.readLine());

		int count=0;
		System.out.println("Even no divisible by 5:");

		for(int i=lower;i<=upper;i++){

			if(i%5==0){

				if(i%2==0){

                                     count++;

					System.out.println(i);
				}
		}
	}
	System.out.println("Count="+count);
}
}

