/*WAP to take range from user and print palindrome numbers>
 *Input:Enter start:100
        Enter end:250
 Output:Palindrome numbers between 100 and 250
101 111 121 131 141
*/

import java.io.*;
class PalindromeNum{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter start");
                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter end");
                int end=Integer.parseInt(br.readLine());

                for(int i=start;i<=end;i++){

                        int temp=i;
                        int rev=0;
                        while(temp!=0){
                        int r=temp%10;
                            rev=rev*10+r;
                             temp=temp/10;
                        }
			if(rev==i)
			{
			        System.out.print(rev+" ");
			}
                }
        }
}
      
