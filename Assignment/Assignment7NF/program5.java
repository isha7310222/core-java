/*WAP to take range as input from user and print perfect numbers.
 *Input:Enter start:1
        Enter end:30
  Output:perfect numbers between 1 to 30 
 6 28
*/
import java.io.*;
class PerfectNumDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter start");
                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter end");
                int end=Integer.parseInt(br.readLine());
                 
                for(int i=start;i<=end;i++){

			int sum=0;

                        for(int j=1;j<i;j++){

				if(i%j==0){
			         sum=sum+j;		
                              }
			}
			      if(sum==i){
                  
                        System.out.println(i+" ");
                        
		       }
			
			

		}
        }
}

