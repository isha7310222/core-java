/*WAP to take range as input from user and print the reveres of all numbers.
 *Input:Enter start:100
        Enter end:200
 Output:Reverse number between 100 and 200
*/
import java.io.*;
class ReverseDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                
		System.out.println("Enter start");
                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter end");
                int end=Integer.parseInt(br.readLine());

                for(int i=start;i<=end;i++){  
                        
			int temp=i;
			int rev=0;
			while(temp!=0){
			int r=temp%10;
			    rev=rev*10+r;
			     temp=temp/10;
			}

			System.out.print(rev+" ");
		}
	}
}	
