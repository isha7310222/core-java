/*WAP to take range as input from user and print perfect square between that range
 *Input:Enter start:1
        Enter end:100
  Output:Perfect square between 1 and 100
  1 4 9 16
*/
import java.io.*;
class PerfectSqrDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter end");
		int end=Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){

			for(int j=1;j*j<=i;j++){

				if(j*j==i){
			System.out.println(i+" ");
			}
		
		}
	}
}
}

