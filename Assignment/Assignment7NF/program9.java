/*WAP to print a series of strong numbers from enterd range.
 * Input:Enter starting number:1
         Enter ending number:150
  Output:
  Strong number between 1 and 150.
  1 2 145
  */
import java.io.*;
class StrongNum{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter start");
                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter end");
                int end=Integer.parseInt(br.readLine());

                for(int i=start;i<=end;i++){
			int temp=i;
			int sum=0;
			while(temp!=0){
			int fact=1;
			int r=temp%10;
			    temp=temp/10;
			for(int j=r;j>=1;j--){

				fact=fact*j;
			}
			sum=sum+fact;
			
			}
			if(i==sum){

				System.out.print(i+" ");
			}
                }
        }
}
