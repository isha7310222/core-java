//WAP to find sum of even no and sum of odd no
import java.io.*;
class ArrDemo3{
        public static void main(String[] args)throws IOException{

         BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

         System.out.println("Enter array size");

         int size=Integer.parseInt(br.readLine());

         int arr[]=new int[size];
         int sum_even=0;
         int sum_odd=0;

         System.out.println("Enter array elements");

         for(int i=0;i<arr.length;i++){
                 arr[i]=Integer.parseInt(br.readLine());
                 if(arr[i]%2==0){
                         sum_even=sum_even+arr[i];
                 }
                 else{
                         sum_odd=sum_odd+arr[i];
                 }

                }
                System.out.println("sum of even integers="+sum_even);
                System.out.println("sum of odd integers="+sum_odd);
        
	}

}
