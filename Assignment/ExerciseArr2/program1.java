//WAP to create an array of n & value n should be taken from user insert values and find sum of all elements in the array
import java.io.*;
class ArrDemo1{
	public static void main(String[] args)throws IOException{

	 BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	
 	 System.out.println("Enter array size");

	 int size=Integer.parseInt(br.readLine());

	 int arr[]=new int[size];
	 int sum=0;

	 System.out.println("Enter array elements");

	 for(int i=0;i<arr.length;i++){
		 arr[i]=Integer.parseInt(br.readLine());

		 sum=sum+arr[i];
		}
		System.out.println("sum of all elements="+sum);
	}
}
