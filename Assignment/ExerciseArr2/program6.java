//WAP to take size and also take integer elements from user find max of the elements from array
import java.io.*;
class ArrDemo6{
        public static void main(String[] args)throws IOException{

         BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

         System.out.println("Enter array size");

         int size=Integer.parseInt(br.readLine());

         int arr[]=new int[size];

         System.out.println("Enter array elements");

         for(int i=0;i<arr.length;i++){

                 arr[i]=Integer.parseInt(br.readLine());

         }
         int max=arr[0];
                for(int j=1;j<arr.length;j++){
                        if(max<arr[j]){
                                max=arr[j];
                        }
                }

                System.out.println("Max element is="+max);

        }


}
