//WAP to take size and elements from user find min element from array
import java.io.*;
class ArrDemo5{
        public static void main(String[] args)throws IOException{

         BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

         System.out.println("Enter array size");

         int size=Integer.parseInt(br.readLine());

         int arr[]=new int[size];

         System.out.println("Enter array elements");

         for(int i=0;i<arr.length;i++){

                 arr[i]=Integer.parseInt(br.readLine());
		 
	 }
	 int min=arr[0];
		for(int j=1;j<arr.length;j++){
			if(min>arr[j]){
				min=arr[j];
			}
		}

		System.out.println("Min element is="+min);
		
	}


}



