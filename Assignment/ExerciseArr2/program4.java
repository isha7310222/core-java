//WAP to serach specific element from array and print its index
import java.io.*;
class ArrDemo4{
        public static void main(String[] args)throws IOException{

         BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

         System.out.println("Enter array size");

         int size=Integer.parseInt(br.readLine());

         int arr[]=new int[size];

         System.out.println("Enter array elements");

         for(int i=0;i<arr.length;i++){
         
	 	 arr[i]=Integer.parseInt(br.readLine());
	}

		 System.out.println("Enter element to be found");
		 int num=Integer.parseInt(br.readLine());


                 for(int i=0;i<arr.length;i++){
			 if(arr[i]==num){
				 System.out.println("Index of element="+i);
				 break;
			 }

                }

        }
}
