/*Take rows and column as user input
 *D4 C3 B2 A1
  A1 B2 C3 D4
  D4 C3 B2 A1
  A1 B2 C3 D3 */

import java.io.*;
class Pattern{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows and column");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){

			char ch=(char)(64+row);
			char ch1='A';
			int temp=row;
			for(int j=1;j<=row;j++){

				if(i%2==1){

					System.out.print(ch+""+temp+" ");
					ch--;
					temp--;
				}
				else{
					System.out.print(ch1+""+j+" ");
				}
			}
			System.out.println();
		}
	}
}
