/*0
 *1 1
  2 3  5
  8 13 21 34*/

import java.io.*;
class Fibonacci{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows");
		int row=Integer.parseInt(br.readLine());

		int a=0;
		int b=1;
		int c;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				c=a;
				System.out.print(c+" ");
				a=b;
				b=b+c;
			}
			System.out.println();
		}
	}
}


