/*WAP to take a number as input & print the addition of factorials of each digit of that no
 *Input:1234
  Output:Addition of factorials of each digit from 1234 is 33.
  */

import java.io.*;
class Factorial{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter any Number ");
		int num=Integer.parseInt(br.readLine());

		int sum=0;
		int temp=0;
		temp=num;

		while(num!=0){

			int r=num%10;
			int fact=1;
			for(int i=1;i<=r;i++){

				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		System.out.println("Addition of factorials of each digit from "+temp+" is "+sum);
	}
}

