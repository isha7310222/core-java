/*WAP to print a series of prime no from entered range.
 *take start and end form user.*/

import java.io.*;
class PrimeNo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start:");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter end:");
		int end=Integer.parseInt(br.readLine());
 
 
		System.out.println("Prime numbers from"+start+"to"+end+"are");
		for(int i=start;i<=end;i++){

			int count=0;
			for(int j=1;j<=i;j++){

				if(i%j==0){

					count++;
				}
				if(count>2){

					break;
				}
			}
			if(count==2){

				System.out.print(i+" ");
			}
		}
	}
}


