/*Take number of rows from user
 *5 4 3 2 1
  8 6 4 2
  9 6
  8 4
  5 */

import java.io.*;
class Pattern3{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows");
		int row=Integer.parseInt(br.readLine());

		int n=1;

		for(int i=1;i<=row;i++){

			for(int j=row-i+1;j>=1;j--){

				int mul=n*j;
				System.out.print(mul+" ");
			}
			n++;
			System.out.println();
		}
	}
}

