/*WAP to print the following pattern
 *O
  14 13
  L  k  J
  9  8  7  6
  E  D  C  B  A */

import java.io.*;
class Pattern4{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row numbers");
		int row=Integer.parseInt(br.readLine());

		int n=(row*(row+1))/2;
		char ch=(char)(64+n);

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

			if(i%2==1){

				System.out.print(ch+" ");
			}
			else{
				System.out.print(n+" ");
			}
			ch--;
			n--;
		}
		System.out.println();
		}
	
	}
}
