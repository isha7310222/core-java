/*WAP & take character if these characters are equal then print as it is
 *but if they are unequal then print their difference.
 (consider positional difference not Ascii value)
Input:a p
Output:The difference between a & p is 15.
*/

import java.io.*;
class Character{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter two character");
		
		char ch1=(char)br.read();
		br.skip(1);
		char ch2=(char)br.read();

		if(ch1==ch2){

			System.out.println(ch1+" "+ch2);
		}
		else if(ch1<=90 && ch2<=90){

			int num1=(char)(ch1-64);
			int num2=(char)(ch2-64);

			int diff=num1-num2;
			if(diff<0){
				diff=diff*-1;
			}
			System.out.println("The difference between "+ch1+" & "+ch2+" is "+diff);
		}
		else{
			int num3=(char)(ch1-96);
			int num4=(char)(ch2-96);

			int diff2=num3-num4;
			if(diff2<0){
                                diff2=diff2*-1;
                        }
			System.out.println("The difference between "+ch1+" & "+ch2+" is "+diff2);
		}

	
	}
}
