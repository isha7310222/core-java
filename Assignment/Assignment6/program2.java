//WAP to ask user to enter no from 0 to 5 and prints its spelling if no is greater than 5 print no is greater than 5
import java.util.Scanner;
class Spelling{
	public static void main(String[] args){
		Scanner obj=new Scanner(System.in);
		
		System.out.println("Enter a no between 0 to 5 of which you want to print spelling");
		int ch=obj.nextInt();
		if(ch>=0){
		switch(ch){
			case 0:
				System.out.println("Zero");
				break;
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;
			case 3:
				System.out.println("Three");
				break;
			case 4:
				System.out.println("Four");
				break;
			case 5:
				System.out.println("Five");
				break;
			default:
				System.out.println("Number is greater than 5");
				break;
		}
		}
		else{
			System.out.println("Negative number not allowed");
		}
	}
}


