/*WAP to find Strong no from array and return its index take size from user
 * I/O..10 25 252 36 564 145
 * O/P..Strong no 145 found at index:5 */
import java.io.*;
class Strong{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		for(int i=0;i<arr.length;i++){
		int temp=arr[i];
		int sum=0;
		while(arr[i]!=0){
			int fact=1;
			int r=arr[i]%10;
			arr[i]=arr[i]/10;
			for(int j=1;j<=r;j++){

				fact=fact*j;
			}
			sum=sum+fact;
		}
		if(sum==temp){
			System.out.println("Strong no "+temp+" "+"found at index :"+i);
		}
		}
	}
}


