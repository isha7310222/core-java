/*WAP to find prime no from array and return its index take size from user
 * I/O..10 25 366 53
 * O/P..Prim8e 53 found at index:3 */
import java.io.*;
class Prime{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                for(int i=0;i<arr.length;i++){
                int count=0;

                for(int j=1;j<=arr[i];j++){

                        if(arr[i]%j==0){

                                count++;
                        }
                }	
                if(count==2){
                        System.out.println("Prime"+" "+arr[i]+" found at index : "+i);
                }

                }
        }
}
