/*WAP to find Palindrome no from array and return its index take size from user
 * I/O..10 25 252 36 564
 * O/P..Palindrome no 252 found at index:2 */
import java.io.*;
class Palindrome{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		for(int i=0;i<arr.length;i++){
		int rev=0;
		int temp=arr[i];
		while(arr[i]!=0){
			int r=arr[i]%10;
			rev=rev*10+r;
			arr[i]=arr[i]/10;
		}
		if(temp==rev){
			System.out.println("Palindrome no "+temp+" "+"found at index :"+i);
		}
		
		}
	}
}
		

