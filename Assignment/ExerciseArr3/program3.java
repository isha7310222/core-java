/*WAP to find composite no from array and return its index take size from user
 * I/O..1 2 3 4 5 6 7
 * O/P..composite 6 found at index:4 */
import java.io.*;
class Composite{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

		for(int i=0;i<arr.length;i++){
		int count=0;

	        for(int j=1;j<arr[i];j++){

			if(arr[i]%j==0){

				count++;
			}
		}
		if(count>2){
			System.out.println("Composite"+" "+arr[i]+" found at index : "+i);
		}
		
		}
	}
}


