/*WAP to print count of digits in elements of array
I/P...02 255 1554
O/P...2  3  4*/
import java.io.*;
class Count{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter array elements");
		
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Number of digits in elements");
		for(int i=0;i<arr.length;i++){
			int count=0;
			while(arr[i]!=0){
				int r=arr[i]%10;
				arr[i]=arr[i]/10;
				count++;
			}
			
			System.out.println(count);
		}

	}
}	
