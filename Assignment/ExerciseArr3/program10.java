/*WAP to print second min element in the array
 * I/O..2 255  1554 15 65
 * O/P..15*/
import java.io.*;
class SecMin{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int min=arr[0];
                int x=0;

                for(int i=0;i<arr.length;i++){

                        for(int j=1;j<arr.length;j++){

                        if(min>arr[j]){
                                x=min;
                                min=arr[j];
                        }

                        }
		}
                        System.out.println("Second min element is "+x);
		

        }
}
