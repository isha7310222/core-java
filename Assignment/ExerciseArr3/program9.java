/*WAP to print second max element in the array
 * I/O..2 255 2 1554 15 65
 * O/P..255*/
import java.io.*;
class SecMax{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		int max=arr[0];
		int temp=0;

		for(int i=0;i<arr.length;i++){

			for(int j=1;j<arr.length;j++){

			if(max<arr[j]){
				temp=max;
				max=arr[j];
			}
			
			}
		}
			System.out.println("Second max element is "+temp);
	
	}
}


			
