/*WAP to find Perfect no from array and return its index take size from user
 * I/O..10 25 252 496 564
 * O/P..Perfect no 496 found at index:3 */
import java.io.*;
class Perfect{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		for(int i=0;i<arr.length;i++){
	        int sum=0;
		for(int j=1;j<arr[i];j++){

			if(arr[i]%j==0){

				sum=sum+j;
			}
		}
		if(sum==arr[i]){
			System.out.println("Perfect no "+arr[i]+" "+"found at index :"+i);
		}
		}
	}
}
 
