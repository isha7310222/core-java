/*WAP to reverse each element in an array take array size from user
 * I/O..10 25 36 678
 * O/P..01 52 63 876*/
import java.io.*;
class Reverse{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Reverse array is :");

		for(int i=0;i<arr.length;i++){
			int rev=0;
			while(arr[i]!=0){
				int r=arr[i]%10;
				   rev=rev*10+r;
				   arr[i]=arr[i]/10;
			}
			System.out.println(rev);
		}
	}
}

